# Design System Build

*It's recommended that you delete the node_modules folder before rebuilding the project dependencies with yarn.*

The following is the standard, basic install to get started. The output in the `dist` folder is not the only code that gets deployed.

- Run `yarn`
- Run `yarn build-lib`

## BUILD

To generate the final output in the `dist` folder, run the bash script file, `build.sh` (`Git Bash` desktop terminal app is preferred on Windows).

The script takes an option flag, `-n`, to remove the `node_modules` folder and clear the npm cache. This is the "nuke" option, and it ensures there are no conflicting or extant dependencies from previous checkouts of the yarn.lock.

After running the build command above, the build script also copies to the `dist` folder:

- `./public`: Vue does not copy these contents when the app is built as a library.
- `src/assets`: Users of the library will need/want access to the images, css and fonts.
- `./design-system-config`: Users of the library will compile Tailwind after the library is installed, not before. This provides the design system's Tailwind configurations to the apps that use the library.
- `./tailwind.config.js`: Provides the entrypoint to the design system's Tailwind configurations.

## DEPLOY

To deploy the design system:

- Make sure the `cabells-design-system-deploy` repo is checked out adjacent to this repo.
- Run the bash script file, `deploy.sh`.
- In the `cabells-design-system-deploy` repo (master branch), add and commit the new/changed files. The commit message should start with the new tag value:
- After pushing the new commit to bitbucket, tag the commit with the new version. You can check the tags to find the latest tag: `git tag`.
- Push the tag to bitbucket.

After that, if your app needs to use the new version, it can update the tag in its package.json.

The `deploy.sh` script takes two option flags, `-n` and `-b` (can be combined e.g., `-bn`). If the `-b` flag is used, the script will run the `build.sh` script. If that happens, and there is an `-n` flag, the build script will receive the `-n` flag and nuke.

## Using the Design System library

The design system is provided to other Vue 3 apps via the `cabells-design-system-deploy` repo. You choose which version of the design system you want to use by indicating the version tag in the `package.json` file. The "deploy" repo provides the library with the app built for library use with a pre-generated `dist` folder.

The `cabells-design-system` repo may be used as well, but requires additional steps, which may be out of purview for the pipelines. To use the `cabells-design-system` repo instead:

- Ensure the `cabells-design-system` repo has a git tag for the commit you want to use.
- Change the `package.json` of your app to use `cabells-design-system`, with the correct version tag.
- Remove the `node_modules` folder.
- Reinstall dependencies: `yarn`.
- Go into the `node_modules/cabells-design-system` folder and run `yarn && yarn build-lib`.
- Build your app as usual: `yarn build`.

## Setup Design System In Your App

- Import CSS in your App.vue file:
`import 'cabells-design-system/src/assets/css/ds/1.0/twglobal.css';`
`import 'cabells-design-system/dist/cabells-design-system.css';`

- Import Design System in main.ts entrypoint:
`import CabellsDesignSystem from 'cabells-design-system';`

- Register Design System with your App in main.ts:
`createApp(App).use(CabellsDesignSystem)`

- Extend Design System Tailwind in your app's tailwind.config.js file:
`const cabellstw = require('cabells-design-system/tailwind.config');`
(Then apply your configuration for CSS purging)

- Register Design System with PostCSS as a plugin, like Tailwind
`require('cabells-design-system/tailwind.config')`

## Code Usage

All the design system components with `<template>` sections can be used in your current single-file-component's `<template>` section without importing. They must be referenced by their kabob-case name, the `name` property of each component.

To access a design system component for a mixin, or access any function defined in the design system `.ts` files, use the following syntax:
`import { MyComponent, myFunction } from 'cabells-design-system`
