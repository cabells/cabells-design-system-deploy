/* eslint-disable no-irregular-whitespace */
const colorConfig = require('./colors.config'); 
const animationConfig = require('./animation.config');
const fontFamilyConfig = require('./fontFamily.config');
const fontSizeConfig = require('./fontSize.config');
const boxShadowConfig = require('./boxShadow.config');
const gridTemplateColumnsConfig = require('./gridTemplateColumns.config');
const gridTemplateRowsConfig = require('./gridTemplateRows.config');
const screensConfig = require('./screens.config');
const spacingConfig = require('./spacing.config');
const lineHeightConfig = require('./lineHeight.config');
const letterSpacingConfig = require('./letterSpacing.config');
const flexGrowConfig = require('./flexGrow.config');
const flexShrinkConfig = require('./flexShrink.config');
const borderRadiusConfig = require('./borderRadius.config');
const borderWidthConfig = require('./borderWidth.config');
const ringWidthConfig = require('./ringWidth.config');
const opacityConfig = require('./opacity.config');
const minmaxWidthConfig = require('./minmaxWidth.config');
const zIndexConfig = require('./zIndex.config');

module.exports = {
  purge: false,
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      minHeight: minmaxWidthConfig,
      maxHeight: minmaxWidthConfig,
      minWidth: minmaxWidthConfig,
      maxWidth: minmaxWidthConfig,
      fontSize: fontSizeConfig,
      lineHeight: lineHeightConfig,
      letterSpacing: letterSpacingConfig,
      spacing: spacingConfig,
      opacity: opacityConfig,
      animation: animationConfig,
      flexGrow: flexGrowConfig,
      flexShrink: flexShrinkConfig,
      boxShadow: boxShadowConfig,
      borderWidth: borderWidthConfig,
      borderRadius: borderRadiusConfig,
      ringWidth: ringWidthConfig,
      gridTemplateColumns: gridTemplateColumnsConfig,
      gridTemplateRows: gridTemplateRowsConfig,
      fontFamily: fontFamilyConfig,
      zIndex: zIndexConfig,
    },
    screens: screensConfig,
    colors: colorConfig,
  },
  variants: {
    extend: {
      position: ['focus', 'focus-visible', 'active', 'hover'],
      inset: ['focus', 'active', 'hover'],
      width: ['focus', 'focus-visible', 'active', 'hover'],
      height: ['focus', 'focus-visible', 'focus-within', 'active', 'hover'],
      space: ['focus', 'active'],
      margin: ['responsive', 'focus', 'focus-visible', 'focus-within', 'active', 'hover'],
      padding: ['responsive', 'focus', 'focus-visible', 'active', 'hover'],
      ringWidth: ['hover', 'focus', 'focus-visible', 'active', 'disabled'],
      ringColor: ['hover', 'focus', 'focus-visible', 'active', 'disabled'],
      ringOpacity: ['hover', 'focus', 'focus-visible','active', 'disabled'],
      borderRadius: ['hover', 'focus', 'focus-visible', 'active', 'disabled'],
      borderWidth: ['hover', 'focus', 'focus-visible', 'active', 'disabled'],
      borderColor: ['hover', 'focus', 'focus-visible', 'active', 'disabled'],
      borderOpacity: ['hover', 'focus', 'focus-visible','active', 'disabled'],
      borderStyle: ['hover', 'focus', 'focus-visible','active', 'disabled'],
      textColor: ['hover', 'focus', 'focus-visible', 'active', 'disabled'],
      backgroundColor: ['hover', 'focus', 'focus-visible', 'active', 'disabled'],
      transitionProperty: ['hover', 'focus', 'active'],
      transitionTimingFunction: ['hover', 'focus', 'active'],
      transitionDelay: ['hover', 'focus', 'active'],
      transitionDuration: ['hover', 'focus', 'active'],
      animation: ['responsive', 'motion-safe', 'motion-reduce'],
      transform: ['hover', 'focus', 'active'],
      cursor: ['hover', 'focus', 'active', 'disabled'],
      visibility: ['hover', 'focus', 'focus-visible', 'active', 'disabled', 'responsive'],
    },
  },

  plugins: [
    require('@tailwindcss/typography'),
  ],
}