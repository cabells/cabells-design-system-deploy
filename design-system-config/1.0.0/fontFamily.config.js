module.exports = {
    sans: ['"Source Sans Pro"', "PT Sans", '-apple-system', 'BlinkMacSystemFont', '"San Francisco"', 'Roboto', '"Segoe UI"', 'sans-serif'],
    display : ['CarterOne', 'Impact'],
    serif: ['Georgia', 'Cambria', '"Times New Roman"', 'Times', 'serif'],
    code: ['Menlo', 'monospace'],
    'snowbird-sharp': ["Snowbird Sharp"],
};