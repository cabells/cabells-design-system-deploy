const spacingConfig = require('./spacing.config');
let minmaxConfig = {
    '128px': '128px',
    '142px': '142px',
};
module.exports = {...spacingConfig, ...minmaxConfig};