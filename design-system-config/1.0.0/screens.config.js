module.exports = {
    'sm': { 'min': '320px', 'max': '767px' }, // handset
    'md': { 'min': '768px', 'max': '1023px' }, // tablet portrait
    'lg': { 'min': '1024px', 'max': '1279px' }, // tablet landscape
    'xl': { 'min': '1280px', 'max': '1919px' }, // laptop (small)
    'xxl': { 'min': '1920px', 'max': '9999px' },
    // 'break1' : { 'min': '500px', 'max': '862px' },
};