module.exports = {
    0: '0px',
    1: '1px',
    2: '2px',
    4: '4px',
    8: '8px',
    16: '16px',
    DEFAULT: '4px',
    'decor-sm': '2px',
    'decor': '4px',
    'decor-lg': '8px',
};