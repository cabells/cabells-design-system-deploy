module.exports = {
    sm: '0px 0.5px 1px rgba(0, 0, 0, 0.03), 0px 0.5px 1.5px rgba(0, 0, 0, 0.05)', /* buttons */
    tooltip_sm: '0px 2px 3px rgba(0, 0, 0, 0.03), 0px 2px 4px rgba(0, 0, 0, 0.05)', /* tooltip, sm -- needs a boost to get beyond the ring width that is needed to make a smooth edge up to the tooltip pointer */
    DEFAULT: '0px 1px 2px rgba(0, 0, 0, 0.06), 0px 1px 3px rgba(0, 0, 0, 0.1)', /* cards */
    md: '0px 2px 4px rgba(0, 0, 0, 0.06), 0px 4px 6px rgba(0, 0, 0, 0.1)', /* dropdowns/alerts */
    lg: '0px 10px 15px rgba(0, 0, 0, 0.1), 0px 4px 6px rgba(0, 0, 0, 0.05)', /* headers */
    xl: '0px 20px 25px rgba(0, 0, 0, 0.1), 0px 10px 10px rgba(0, 0, 0, 0.04)', /* modals */
    xxl: '0px 25px 50px rgba(0, 0, 0, 0.25)', /* drag and drop */
    inner: 'inset 0px 2px 4px rgba(0, 0, 0, 0.06)',
    card: '0px 10px 15px -3px rgba(0, 0, 0, 0.1), 0px 4px 6px -2px rgba(0, 0, 0, 0.05)', /* marketing cards */
    input_hover: 'inset 0px 1px 2px rgba(0, 0, 0, 0.05)',
};