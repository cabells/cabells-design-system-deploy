module.exports = {
    'tiny': '0.0156rem',
    'mini': '0.0168rem',
    'micro': '0.0132rem',
    'sm': '0.0192rem',
    'h5': '0.0228rem',
	'h6': '0.025rem',
    'md': '0.0266rem',
    'h4': '0.0276rem',
    'blockquote': '0.0276rem',
    DEFAULT: '0.0322em',
    'h3': '0.0336rem',
	'display-medium': '0.0392rem',
    'h2': '0.0396rem',
    'h1': '0.048rem',
    'lg': '0.056rem',
    'header-title': '0.2842rem',
};