const colors = require('tailwindcss/colors'); 

module.exports = {
    transparent: 'transparent',
    current: 'currentColor',
    snow: colors.white,
    drift: '#F4F7F9',
    blueIce: '#C8D8E5',
    winterNight:'#4D4D4D',
    midnightMatter:'#343434',/*gray-800*/
    nightCloud: '#606060',/*gray-600*/
    twilightStratus:'#AEAEAE',/*gray-400*/
    dustJacket:'#EAEAEA',/*grey-200*/
    amethystShine: '#F4F1F9',
    lilac: '#DDD6FE',
    lavender: '#A78BFA',
    manganese: '#7C3AED',
    aubergine: '#5B21B6',
    firesideDrink:'#80220F',
    waveFoam:'#EFF1FB',/*indigo-50*/
    tranquilSea:'#A0AEE4',/*indigo-200*/
    liquidWisdom:'#5069CE',/*indigo-400*/
    oceanic:'#314BAF',/*indigo-600*/
    fullFathom:'#1F2F6F',/*indigo-800*/
    red: {
        50:'#F9F0F0',
        200:'#D89797',
        400:'#BF5F5F',
        DEFAULT:'#923A3A',
        800:'#582323'
    },
    orange: {
        50:'#FCF7EE',
        200:'#EAC786',
        400:'#BF8822',
        DEFAULT:'#AD7B1F',
        800:'#684A12',
    },
    gold: '#D8D85A',
    fireyWarning:'#C6510C',
    retroBlue:'#9DB9CC',/*blue-200*/
    winterNoon:'#6B95B3',/*blue-400*/
    winterEvening:'#456B87',/*blue-600*/
    winterDusk:'#2A4151',/*blue-800*/
    green: {
        50:'#F2F7F4',
        200:'#A5CAB2',
        400:'#5B9A71',
        DEFAULT:'#4B815E',
        800:'#34724B',
    },
    naturalGray: {
        50:'#FAFAFA',/*gray-50*/
        DEFAULT: '#FAFAFA',/*gray-50*/
        200:'#EAEAEA',/*gray-200*/
        400:'#AEAEAE',/*gray-400*/
        800:'#343434',/*gray-800*/
    },
    silver:'#C4C4C4',
	sunshine:'#E8E89C',/*yellow-200*/
};