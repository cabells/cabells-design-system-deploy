module.exports = {
    'dropdown-enter-active': 'dropdown__y__1 150ms linear forwards',
    'dropdown-leave-active': 'dropdown__y__3 150ms ease-out forwards',
    'journal-card-expand': 'Journal-card-preview-1__height__2 350ms ease-in forwards',
    'journal-card-collapse': 'Journal-card-preview-1__height__4 350ms ease-in forwards',
    spinner: 'rotate 1.5s linear infinite',
};