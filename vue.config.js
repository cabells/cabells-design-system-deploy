const path = require('path');
module.exports = {
    outputDir: './dist',
    productionSourceMap: false,
    css: { extract: false },
    devServer: {
      allowedHosts: 'all',
      compress: true,
      port: 443,
      client: {
        webSocketURL: 'wss://cabells-journalytics-dev.azurewebsites.net:443/ws',
      },
    },
    chainWebpack: config => {
      config.module
        .rule('vue')
        .use('vue-loader')
        .loader('vue-loader')
        .tap(options => {
          options.transformAssetUrls = {
            img: 'src',
            image: 'xlink:href',
          }
  
          return options
        }),
      config.module.entryPoints = {
        index: {
            import: path.resolve(__dirname, 'src/main.ts'),
            dependOn: 'all',
        },
        lib: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/lib/1.0.0/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/lib/1.0.0/*.vue'),
            ],
        },
        anchors: {
            import: path.resolve(__dirname, 'src/components/ds/1.0/anchors/1.0.0/*.ts'),
            import: path.resolve(__dirname, 'src/components/ds/1.0/anchors/1.0.0/*.vue'),
            dependOn: 'lib',
        },
        avatars: {
            import: path.resolve(__dirname, 'src/components/ds/1.0/avatars/1.0.0/*.ts'),
            import: path.resolve(__dirname, 'src/components/ds/1.0/avatars/1.0.0/*.vue'),
            dependOn: 'lib',
        },
        buttons: {
            import: [
              path.resolve(__dirname, 'src/components/ds/1.0/buttons/1.0.0/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/buttons/1.0.0/*.vue'),
              path.resolve(__dirname, 'src/components/ds/1.0/buttons/1.0.0/**/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/buttons/1.0.0/**/*.vue'),
              path.resolve(__dirname, 'src/components/ds/1.0/buttons/1.0.0/**/**/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/buttons/1.0.0/**/**/*.vue'),
            ],
            dependOn: ['lib', 'icons'],
        },
        cards: {
            import: [
              path.resolve(__dirname, 'src/components/ds/1.0/cards/1.0.0/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/cards/1.0.0/*.vue'),
              path.resolve(__dirname, 'src/components/ds/1.0/cards/1.0.0/**/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/cards/1.0.0/**/*.vue'),
              path.resolve(__dirname, 'src/components/ds/1.0/cards/1.0.0/**/**/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/cards/1.0.0/**/**/*.vue'),
            ],
            dependOn: 'lib',
        },
        charts: {
            import: [
              path.resolve(__dirname, 'src/components/ds/1.0/charts/1.0.0/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/charts/1.0.0/*.vue'),
              path.resolve(__dirname, 'src/components/ds/1.0/charts/1.0.0/**/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/charts/1.0.0/**/*.vue'),
            ],
            dependOn: 'lib',
        },
        data: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/*.vue'),
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/**/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/**/*.vue'),
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/**/**/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/**/**/*.vue'),
            ],
            dependOn: 'lib',
        },
        dividers: {
            import: [
              path.resolve(__dirname, 'src/components/ds/1.0/dividers/1.0.0/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/dividers/1.0.0/*.vue'),
              path.resolve(__dirname, 'src/components/ds/1.0/dividers/1.0.0/**/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/dividers/1.0.0/**/*.vue'),
            ],
            dependOn: 'lib',
        },
        dropdown: {
            import: [
              path.resolve(__dirname, 'src/components/ds/1.0/dropdown/1.0.0/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/dropdown/1.0.0/*.vue'),
              path.resolve(__dirname, 'src/components/ds/1.0/dropdown/1.0.0/**/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/dropdown/1.0.0/**/*.vue'),
            ],
            dependOn: ['lib', 'buttons'],
        },
        flex: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/flex/1.0.0/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/flex/1.0.0/*.vue'),
                path.resolve(__dirname, 'src/components/ds/1.0/flex/1.0.0/**/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/flex/1.0.0/**/*.vue'),
            ],
            dependOn: 'lib',
        },
        grid: {
            import: [
                path.resolve(__dirname, '/src/components/ds/1.0/grid/1.0.0/*.ts'),
                path.resolve(__dirname, '/src/components/ds/1.0/grid/1.0.0/*.vue'),
                path.resolve(__dirname, '/src/components/ds/1.0/grid/1.0.0/**/*.ts'),
                path.resolve(__dirname, '/src/components/ds/1.0/grid/1.0.0/**/*.vue'),
            ],
            dependOn: 'lib',
        },
        icons: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/icons/1.0.0/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/icons/1.0.0/*.vue'),
                path.resolve(__dirname, 'src/components/ds/1.0/icons/1.0.0/lib/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/icons/1.0.0/lib/*.vue'),
            ],
            dependOn: 'lib',
        },
        images: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/images/1.0.0/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/images/1.0.0/*.vue'),
                path.resolve(__dirname, 'src/components/ds/1.0/images/1.0.0/**/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/images/1.0.0/**/*.vue'),
            ],
            dependOn: 'lib',
        },
        inputs: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/inputs/1.0.0/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/inputs/1.0.0/*.vue'),
                path.resolve(__dirname, 'src/components/ds/1.0/inputs/1.0.0/lib/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/inputs/1.0.0/lib/*.vue'),
            ],
            dependOn: ['lib', 'icons'],
        },
        tags: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/tags/1.0.0/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/tags/1.0.0/*.vue'),
                path.resolve(__dirname, 'src/components/ds/1.0/tags/1.0.0/**/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/tags/1.0.0/**/*.vue'),
            ],
            dependOn: 'lib',
        },
        tooltips: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/tooltips/1.0.0/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/tooltips/1.0.0/*.vue'),
                path.resolve(__dirname, 'src/components/ds/1.0/tooltips/1.0.0/lib/*.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/tooltips/1.0.0/lib/*.vue'),
            ],
            dependOn: ['lib', 'icons', 'tags'],
        },
        typography: {
            import: [
              path.resolve(__dirname, 'src/components/ds/1.0/typography/1.0.0/*.ts'),
              path.resolve(__dirname, 'src/components/ds/1.0/typography/1.0.0/*vue'),
            ],
            dependOn: 'lib',
        },
        twglobal: {
            import: path.resolve(__dirname, 'src/assets/css/ds/1.0/twglobal.css'),
            dependOn: [
                'styles',
                'keyframes',
            ],
        },
        configs: {
            import: [
                path.resolve(__dirname, 'design-system-config/1.0.0/*.config.js'),
            ],
        },
        styles: {
            import: [
                path.resolve(__dirname, 'src/assets/css/ds/1.0/*.css'),
            ],
        },
        keyframes: {
            import: [
                path.resolve(__dirname, 'src/assets/css/ds/1.0/keyframes/*.css'),
            ],
        },
        fonts: {
            import: [
                path.resolve(__dirname, 'src/assets/fonts/*.json'),
            ],
        },
      },
      config.optimization.runtimeChunk = 'single';
      config.optimization.flagIncludedChunks = true;
      config.optimization.realContentHash = true;
      config.optimization.removeAvailableModules = false;
      config.optimization.splitChunks.chunks = 'all';
      
      config.optimization.minimizer('terser').tap((args) => {
        args[0].terserOptions.output = {
          comments: false
        }
        return args
      })
    },
};