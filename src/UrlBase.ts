const handleSlashes = (uri: string, env?: string) : string => {
   if (uri && uri.length > 1 && !uri.startsWith('/') && !env?.endsWith('/')) {
      uri = '/' + uri;
   }
   return uri;
}

const getCmsUrl = (uri: string, slug?: string, id?: string) : string => {
   uri = handleSlashes(uri, process.env.VUE_APP_CMS_API_URL);
   let url = process.env.VUE_APP_CMS_API_URL + uri;
   if (slug && slug.length > 0) {
      url += '?slug=' + slug;
   } else if (id && id.length > 0) {
      id = handleSlashes(id, process.env.VUE_APP_CMS_API_URL);
      url += id;
   }
   return url;
};

const getCabellsMedicineApiUrl = (uri: string) : string => { 
   uri = handleSlashes(uri, process.env.VUE_APP_MEDICINE_API_URL);
   return process.env.VUE_APP_MEDICINE_API_URL + uri;
};

const getCrmApiUrl = (uri: string) : string => {
   uri = handleSlashes(uri, process.env.VUE_APP_CRM_API_URL);
	return process.env.VUE_APP_CRM_API_URL + uri;
};

const getApiHeaders = {
   headers: {
      'Content-Type': 'application/json',
   }
};

export {
   getCmsUrl,
   getCabellsMedicineApiUrl,
   getApiHeaders,
   getCrmApiUrl,
}