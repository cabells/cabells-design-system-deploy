import PrimitiveJournalCardSection from './PrimitiveJournalCardSection.vue';

// segments
import DashboardSegment from './DashboardSegment.vue';
import JournalStatSegment from './JournalStatSegment.vue';
import IconSegmentRight from './IconSegmentRight.vue';

import JournalStatGrouping from './JournalStatGrouping.vue';
import JournalStatRowGrouping from './JournalStatRowGrouping.vue';
import JournalStatAnchorGrouping from './JournalStatAnchorGrouping.vue';

export const sections = {
    PrimitiveJournalCardSection,

    DashboardSegment,
    JournalStatSegment,
    IconSegmentRight,
    JournalStatGrouping,
    JournalStatRowGrouping,
    JournalStatAnchorGrouping,
    
    [Symbol.iterator]: function* () {
        yield* Object.values(this);
    }
}