const snowbird = require('../../../../../../assets/fonts/snowbird-font.json');
const glyphs = snowbird['glyphs'];

/**
 * Get font icon class from the given icon string
 * 
 * @param icon string|null
 * @returns string
 */
const getIconClass = (icon?: string) : string => {
    if (!icon || icon.length < 1) {
        return '';
    }
    const prefix = snowbird.css_prefix_text;
    for (const glyph in glyphs) {
        if (icon === glyphs[glyph].css) {
            return prefix + icon;
        }
    }
    return '';
};

/**
 * Get font icon char code from the given icon string
 * 
 * @param icon string|null
 * @returns string
 */
const getIconCharFromCode = (icon?: string) : string => {
    if (!icon || icon.length < 1) {
        return '';
    }
    for (const glyph in glyphs) {
        if (icon === glyphs[glyph].css) {
            return String.fromCharCode(glyphs[glyph].code);
        }
    }
    return '';
};

export {
    getIconClass,
    getIconCharFromCode,
}