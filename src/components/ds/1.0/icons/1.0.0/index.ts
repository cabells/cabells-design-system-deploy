import IconBase from './lib/IconBase.vue';
import PrimitiveIcon from './PrimitiveIcon.vue';
import PredatoryIcon from './PredatoryIcon.vue';
import Icon from './Icon.vue';
import IconXs from './IconXs.vue';
import IconXl from './IconXl.vue';
import Icon4 from './Icon4.vue';
import Icon6 from './Icon6.vue';
import Icon8 from './Icon8.vue';
import IconFontText from './IconFontText.vue';
import IconText from './IconText.vue';
import IconTooltip from './IconTooltip.vue';

export {
    IconBase,
    PrimitiveIcon,
    PredatoryIcon,
    Icon,
    IconXs,
    IconXl,
    Icon4,
    Icon6,
    Icon8,
    IconFontText,
    IconText,
    IconTooltip,
}