import ImgBase from './lib/ImgBase.vue';
import PrimitiveImage from './PrimitiveImage.vue';
import ImageGradient from './ImageGradient.vue';

// svg logos
import AltmetricsLogo from './svg/AltmetricsLogo.vue';
import SciteLogo from './svg/SciteLogo.vue';
import CabellsLogo from './svg/CabellsLogo.vue';

// svg icons
import GreenCheckCircle from './svg/GreenCheckCircle.vue';
import BanCircle from './svg/BanCircle.vue';
import HelpCircle from './svg/GreenCheckCircle.vue';

// svg badges
import GreenOaBadge from './svg/GreenOaBadge.vue';
import GreenOaBadgeSmall from './svg/GreenOaBadgeSmall.vue';

export {
    ImgBase,
    PrimitiveImage,
    ImageGradient,

    AltmetricsLogo,
    SciteLogo,
    CabellsLogo,

    GreenCheckCircle,
    BanCircle,
    HelpCircle,

    GreenOaBadge,
    GreenOaBadgeSmall,
}