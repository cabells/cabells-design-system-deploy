import PrimitiveJournalCardSection from './PrimitiveJournalCardSection.vue';

// segments
import DashboardSegment from './segments/DashboardSegment.vue';
import JournalStatSegment from './segments/JournalStatSegment.vue';
import IconSegmentRight from './segments/IconSegmentRight.vue';

import JournalStatGrouping from './segments/JournalStatGrouping.vue';
import JournalStatRowGrouping from './segments/JournalStatRowGrouping.vue';
import JournalStatAnchorGrouping from './segments/JournalStatAnchorGrouping.vue';

export const sections = {
    PrimitiveJournalCardSection,

    DashboardSegment,
    JournalStatSegment,
    IconSegmentRight,
    JournalStatGrouping,
    JournalStatRowGrouping,
    JournalStatAnchorGrouping,
    
    [Symbol.iterator]: function* () {
        yield* Object.values(this);
    }
}