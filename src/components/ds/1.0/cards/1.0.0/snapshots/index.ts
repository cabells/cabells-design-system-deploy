import CciSnapshot from './CciSnapshot.vue';
import KpiSnapshot from './KpiSnapshot.vue';
import MetricSnapshot from './MetricSnapshot.vue';
import PrimitiveSnapshot from './PrimitiveSnapshot.vue';
import PredatorySnapshotLarge from './PredatorySnapshotLarge.vue';

export {
    CciSnapshot,
    KpiSnapshot,
    MetricSnapshot,
    PrimitiveSnapshot,
    PredatorySnapshotLarge,
}