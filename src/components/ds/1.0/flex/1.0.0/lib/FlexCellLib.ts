import { parseState } from "../../../lib/1.0.0/StyleBaseLib";

/**
 * Get the flex order by the greater of the given 
 * order value and the limit, acquired via string
 * TODO: implement screen variants (not needed now)
 * 
 * @param order string
 * @param limit string
 * @returns string
 */
const getOrder = (order = '', limit = '12') => {
    if (order.length < 1) {
        return '';
    }
    if (parseInt(order) < 1) {
        return 'cds-order-none';
    }
    const flexOrder = _limit(order, limit);
    return 'cds-order-' + flexOrder;
};

/**
 * Get the Tailwind style for the given css animatable type 
 * supported by Tailwind and this app [grow, shrink]
 * 
 * @param state string
 * @param type string
 * @param value string
 * @param limit string
 * @returns string
 */
const getFlexAnimatable = (state = '', type: string, value = '0', limit = '5') : string => {
    const styleValue = _limit(value, limit);
    const prefix = 'cds-flex-';
    const animatables = ['grow', 'shrink']; // flex-basis not supported or needed
    if (!animatables.includes(type) || value.length < 1) {
        return '';
    }

    const style = prefix + type;
    if (styleValue === 1) {
        return style;
    }

    return parseState(state) + style + '-' + styleValue;
};

/**
 * Get a composite string of Tailwind styles for all screens
 * by the given width ratio
 * 
 * @param state string
 * @param ratio string|null The fraction of the flex row this cell will consume
 * @param def string
 * @returns string
 */
const getFlexWidth = (state = '', ratio?: string, def = '') : string => {
    ratio = ratio ?? def;
    if (ratio.length < 1) {
        return '';
    }
    if (parseInt(ratio) === 1) {
        return 'cds-w-full';
    }
    const baseStyle = 'cds-w-1/';
    let screenRatio = '';
    if (state == 'sm') {
        screenRatio = getSmScreen(ratio);
    } else {
        screenRatio = getDefScreen(ratio);
    }
    const style = baseStyle + screenRatio;
    return parseState(state) + style;
};

/**
 * Get the flex style by the given type, or the 
 * default value if the given style type is not allowed.
 * 
 * @param state string
 * @param styleType string
 * @param val string
 * @param def string
 * @param prefix string
 * @returns string
 */
const getFlexStyle = (
    state: string,
    styleType: string,
    prefix: string,
    val: string,
    def = '', 
    
) : string =>  {
    const allowedList = getAllowedList(styleType);
    val = val ?? def;
    if (val.length > 0 && allowedList.includes(val)) {
        return parseState(state) + prefix + val;
    }
    return '';
};

/**
 * Get justify styles for flex
 * 
 * @param state string
 * @param styleType string
 * @param val string
 * @param def string
 * @returns string
 */
const getJustifyStyle = (state = '', styleType: string, val: string, def = '') : string => {
    const prefix = 'cds-justify-'
    return getFlexStyle(state, styleType, prefix, val, def);
};

/**
 * Get align styles for flex
 * 
 * @param state string
 * @param styleType string
 * @param val string
 * @param def string
 * @returns string
 */
const getAlignStyle = (state = '', styleType: string, val: string, def = '') : string => {
    const allowedList = ['content', 'items', 'self'];
    if (!allowedList.includes(styleType)) {
        return '';
    }
    const prefix = 'cds-' + styleType + '-'
    return getFlexStyle(state, styleType, prefix, val, def);
};

/**
 * Get list of allowed flex styles supported by 
 * Tailwind and this app. Styles for: [content, items, self, align].
 * 
 * @param styleType string|null
 * @returns string[]
 */
const getAllowedList = (styleType?: string) : string[] => {
    switch (styleType) {
        case 'content': {
            return ['start', 'end', 'evenly', 'between', 'around', 'center'];
        }
        case 'items': {
            return ['start', 'end', 'center', 'stretch'];
        }
        case 'self': {
            return ['auto', 'start', 'end', 'center', 'stretch'];
        }
        default: {
            return [];
        }
    }
};

/**
 * Get the small screen's flex cell (column) limit
 * 
 * @param value string
 * @returns string
 */
const getSmScreen = (value: string) : string => {
    const screenValue = parseInt(value);
    return screenValue > 4 ? '4' : value;
};

/**
 * Get the default screen's flex cell (column) limit
 * 
 * @param value string
 * @returns string
 */
const getDefScreen = (value: string) : string => {
    const screenValue = parseInt(value);
    return screenValue > 6 ? '6' : value;
};

/**
 * Get the greater of two numerical values acquired via string
 * 
 * @param value string
 * @param limit string
 * @returns number
 */
const _limit = (value: string, limit: string) : number => {
    if (value.length < 1 || limit.length < 1) {
        return 0;
    }
    const absValue = Math.abs(parseInt(value));
    const absLimit = Math.abs(parseInt(limit));
    return absValue > absLimit ? absLimit : absValue;
};

export {
    getDefScreen,
    getSmScreen,
    getAllowedList,
    getFlexStyle,
    getFlexWidth,
    getFlexAnimatable,
    getOrder,
    getJustifyStyle,
    getAlignStyle,
}