import { getStyle } from './StyleBaseLib';

/**
 * Get the padding style
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string 
 */
 const getPadding = (state = '', val?: string, def?: string) : string => {
    return getStyle(state, false, 'p', val, def);
};

/**
 * Get padding-top style 
 * 
 * @param state string
 * @param val string|null
 * @param def string Default padding top value
 * @returns string
 */
const getPaddingTop = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'pt', val, def);
};

/**
 * Get padding-bottom style 
 * 
 * @param state string
 * @param val string|null
 * @param def string Default padding bottom value
 * @returns string
 */
const getPaddingBottom = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'pb', val, def);
};

/**
 * Get padding-left style 
 * 
 * @param state string
 * @param val string|null
 * @param def string Default padding left value
 * @returns string
 */
const getPaddingLeft = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'pl', val, def);
};

/**
 * Get padding-right style 
 * 
 * @param state string
 * @param val string|null
 * @param def string Default padding right value
 * @returns string
 */
const getPaddingRight = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'pr', val, def);
};

/**
 * Get padding-left and padding-right style 
 * 
 * @param state string
 * @param val string|null
 * @param def string Default padding left/right value
 * @returns string
 */
const getPaddingX = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'px', val, def);
};

/**
 * Get padding-top and padding-bottom style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default padding top/bottom value
 * @returns string
 */
const getPaddingY = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'py', val, def);
};

export {
    getPadding,
    getPaddingTop,
    getPaddingBottom,
    getPaddingLeft,
    getPaddingRight,
    getPaddingX,
    getPaddingY,
}