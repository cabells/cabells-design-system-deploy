import { getAllowedStyle, parseState, } from './StyleBaseLib';

/**
 * Get align-items style 
 * 
 * @param state string
 * @param val string
 * @param def string Default item align value
 * @returns string
 */
 const getItemAlign = (state = '', val?: string, def = 'center') : string => {
    const allowed = ['start', 'end', 'center', 'stretch', 'baseline'];
    val = val ?? def;
    if (val && val.length > 0) {
        return getAllowedStyle(state, false, 'items', allowed, val, def);
    }
    return '';
};

/**
 * Get the Tailwind style for the given flex direction
 * 
 * @param state string
 * @param direction string The flex direction style [column, row, and reverse variations]
 * @param def string
 * @returns string
 */
 const getFlexDirection = (state = '', direction?: string, def = '') : string => {
    let style = '';
    direction = direction ?? def;
    if (!direction || direction.length < 1) {
        return '';
    }
    switch (direction) {
        case 'column':
        case 'col': {
            style = 'col';
            break;
        }
        case 'rrev':
        case 'row-reverse': {
            style = 'row-reverse';
            break;
        }
        case 'crev':
        case 'col-reverse': {
            style = 'col-reverse';
            break;
        }
        case 'row': 
        default: {
            style = 'row';
            break;
        }
    }
    return style.length > 0 ? parseState(state) + 'cds-flex-' + style : '';
};

/**
 * Get the Tailwind style for the given flex wrap
 * 
 * @param state string
 * @param wrap string|null The flex wrap style [reverse, wrap, nowrap]
 * @param def string
 * @returns string
 */
const getFlexWrap = (state = '', wrap?: string, def = '') : string => {
    let style = '';
    wrap = wrap ?? def;
    if (!wrap || wrap.length < 1) {
        return '';
    }
    switch (wrap) {
        case 'rev': 
        case 'reverse': {
            style = 'wrap-reverse';
            break;
        }
        case 'wrap': {
            style = 'wrap';
            break;
        }
        case 'nowrap': {
            style = 'nowrap';
            break;
        }
        default: {
            break;
        }
    }
    return style.length > 0 ? parseState(state) + 'cds-flex-' + style : '';
};

export {
    getItemAlign,
    getFlexDirection,
    getFlexWrap,
}