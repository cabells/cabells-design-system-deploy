import { getStyle, getAllowedRootStyle, } from './StyleBaseLib';

/**
 * Get position style
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string
 */
 const getPosition = (state = '', val?: string, def?: string) : string => {
    const allowed = ['absolute', 'relative', 'fixed', 'static', 'sticky'];
    return getAllowedRootStyle(state, false, allowed, val, def);
};

/**
 * Get position left style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 */
const getLeft = (state = '', val?: string, def = '') : string => {
    return getStyle(state, false, 'left', val, def);
};

/**
 * Get position right style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 */
const getRight = (state = '', val?: string, def = '') : string => {
    return getStyle(state, false, 'right', val, def);
};

/**
 * Get position top style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 */
const getTop = (state = '', val?: string, def = '') : string => {
    return getStyle(state, false, 'top', val, def);
};

/**
 * Get position bottom style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 */
const getBottom = (state = '', val?: string, def = '') : string => {
    return getStyle(state, false, 'bottom', val, def);
};

export {
    getPosition,
    getTop,
    getBottom,
    getLeft,
    getRight,
}