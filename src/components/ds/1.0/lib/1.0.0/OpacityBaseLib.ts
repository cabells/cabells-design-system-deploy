const opacityConfig = require('../../../../../../design-system-config/1.0.0/opacity.config');
const opacityStyles = Object.keys(opacityConfig)
                            .concat(['0', '5', '10', '20', '25', '30', '40', '50', '60', '70', '80', '90', '100']);

import { getAllowedStyle } from '../../lib/1.0.0/StyleBaseLib';

/**
 * Get opacity style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default border opacity value
 * @returns string
 */
 const getOpacity = (state = '', val?: string, def = '') : string => {
    return getAllowedStyle(state, false, 'opacity', opacityStyles, val, def);
};

/**
 * Get bg opacity style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default border opacity value
 * @returns string
 */
 const getBgOpacity = (state = '', val?: string, def = '') : string => {
    return getAllowedStyle(state, false, 'bg-opacity', opacityStyles, val, def);
};

/**
 * Get text opacity style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default border opacity value
 * @returns string
 */
 const getTextOpacity = (state = '', val?: string, def = '') : string => {
    return getAllowedStyle(state, false, 'text-opacity', opacityStyles, val, def);
};

/**
 * Get border opacity style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default border opacity value
 * @returns string
 */
 const getBorderOpacity = (state = '', val?: string, def = '') : string => {
    return getAllowedStyle(state, false, 'border-opacity', opacityStyles, val, def);
};

/**
 * Get divide opacity style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default border opacity value
 * @returns string
 */
 const getDivideOpacity = (state = '', val?: string, def = '') : string => {
    return getAllowedStyle(state, false, 'divide-opacity', opacityStyles, val, def);
};

/**
 * Get ring opacity style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default border opacity value
 * @returns string
 */
 const getRingOpacity = (state = '', val?: string, def = '') : string => {
    return getAllowedStyle(state, false, 'ring-opacity', opacityStyles, val, def);
};

export {
    getOpacity,
    getBgOpacity,
    getTextOpacity,
    getBorderOpacity,
    getDivideOpacity,
    getRingOpacity,
    opacityStyles,
}