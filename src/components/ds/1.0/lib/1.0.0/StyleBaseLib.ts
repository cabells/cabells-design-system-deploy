const colorConfig = require('../../../../../../design-system-config/1.0.0/colors.config');

/**
 * Check if style is set to the DEFAULT value, 
 * and prepare the style suffix accordingly.
 * DEFAULT styles are produced by Tailwind
 * 
 * @param style string
 * @returns string
 */
const checkDefault = (style = '') : string => {
    style = style === 'default' ? 'DEFAULT' : style;
    return (style === 'DEFAULT') ? '' : '-' + style;
};

/**
 * Check if a given property exists in a given array
 * 
 * @param styleNames array
 * @param property string
 * @returns boolean
 */
const isValidStyle = (styleNames: string[], property = '') : boolean => {
    // check if array, due to potential whitespace issues importing module files (e.g., color config)
    return (Array.isArray(styleNames) && styleNames.includes(property));
};

/**
 * Get a valid color style by context, with appropriate state prefix
 * 
 * @param state string
 * @param disabled boolean
 * @param val string|null
 * @param def string|null
 * @param context string
 * @returns string
 */
const getValidColor = (
    state: string, 
    disabled = false, 
    val?: string, 
    def?: string, 
    context = 'text'
) : string => {
    if (disabled && state != 'disabled') {
        return '';
    }
    val = val ?? def ?? '';
    const allowed = Object.keys(colorConfig);
    allowed.push('gradient');
    // if a style definition is nested in an object, Tailwind drills down through hyphens
    // so grab the first segment to get the base color definition
    const valSegments = val.split('-');
    const leadingStyleName = valSegments[0];  
    if (val.length && isValidStyle(allowed, leadingStyleName)) {
        const secondaryStyleName = valSegments[1] ? '-' + valSegments[1] : '';
        val = leadingStyleName + secondaryStyleName;
        if (valSegments.length === 3) {
            val += '-' + valSegments[2];
        }
        return parseState(state) + 'cds-' + context + '-' + val;
    }
    return '';
};

/**
 * Prepare style prefixes for the given CSS state.
 * Blank or default/DEFAULT values return blank state.
 * 
 * @param state string HTML state, e.g., hover, disabled, etc.
 * @returns string
 */
const parseState = (state = '') : string => {
    let elementState = '';
    if (state.length > 1 && state.toLowerCase() != 'default') {
        elementState = state + ':';
    }
    return elementState;
};

/**
 * Tailwind style factory
 * 
 * @param state string
 * @param disabled boolean
 * @param style string
 * @param val string|null
 * @param def string|null
 * @returns string
 */
const getStyle = (
    state = '', 
    disabled = false, 
    style: string, 
    val?: string, 
    def?: string
) : string => {
    if (disabled && state != 'disabled') {
        return '';
    }
    val = val ?? def;
    if (val && val.length > 0) {
        if (val.startsWith('-')) {
            style = '-' + style;
            val = val.slice(1, val.length);
        }
        return parseState(state) + 'cds-' + style + checkDefault(val);
    }
    return '';
};

/**
 * Tailwind style factory with allowedlist validation
 * 
 * @param state string
 * @param disabled boolean
 * @param style string
 * @param allowed string[]
 * @param val string|null
 * @param def string|null
 * @returns string
 */
const getAllowedStyle = (
    state = '', 
    disabled = false, 
    style: string, 
    allowed: string[], 
    val?: string, 
    def?: string,
    defaultCheck = true
) : string => {
    if (disabled && state != 'disabled') {
        return '';
    }
    val = val ?? def;
    if (val && isValidStyle(allowed, val)) {
        if (val.startsWith('-')) {
            style = '-' + style;
            val = val.slice(1, val.length);
        }
        const allowedStyle = parseState(state) + 'cds-' + style;
        val = defaultCheck ? checkDefault(val) : val == 'DEFAULT' ? '-default' : '-' + val;
        return allowedStyle + val;
    }
    return '';
};

/**
 * Tailwind style factory with allowedlist validation
 * using the value as the style
 * 
 * @param state string
 * @param disabled boolean
 * @param style string
 * @param allowed string[]
 * @param val string|null
 * @param def string|null
 * @returns string
 */
const getAllowedRootStyle = (
    state = '', 
    disabled = false, 
    allowed: string[], 
    val?: string, 
    def?: string
) : string => {
    if (disabled && state != 'disabled') {
        return '';
    }
    val = val ?? def;
    if (val && isValidStyle(allowed, val)) {
        return parseState(state) + 'cds-' + val;
    }
    return '';
};

/**
 * Get the corresponding OpenAccess color by OpenAccess type
 * 
 * @param OpenAccessType string
 * @returns string
 */
const getOpenAccessColor = (OpenAccessType = '') : string => {
    let color = '';
    if (OpenAccessType?.startsWith('Diamond')) {
        OpenAccessType = 'Diamond';
    }
    switch(OpenAccessType) {
        case 'Hybrid': {
            color = 'manganese';
            break;
        }
        case 'Gold': {
            color = 'gold';
            break;
        }
        case 'Diamond':
        case 'Bronze': {
            color = 'gradient-arctic-nights';
            break;
        }
        case 'Subscription': {
            color = 'fullFathom';
            break;
        }
        case 'Green': {
            color = 'green-400';
            break;
        }
        default: {
            color = 'snow';
            break;
        }
    }
    return color;
};

/**
 * Get the corresponding OpenAccess opacity by OpenAccess type
 * 
 * @param OpenAccessType string
 * @returns string
 */
const getOpenAccessOpacity = (OpenAccessType = '') : string => {
    if (OpenAccessType?.startsWith('Diamond')) {
        OpenAccessType = 'Diamond';
    }
    switch(OpenAccessType) {
        case 'Hybrid': 
        case 'Gold': 
        case 'Bronze': 
        case 'Subscription': {
            return '100';
        }
        case 'Diamond': {
            return '30';
        }
        case 'Green': 
        default: {
            return 'green-400';
        }
    }
};

export {
    checkDefault,
    isValidStyle,
    parseState,
    getStyle,
    getAllowedStyle,
    getAllowedRootStyle,
    getValidColor,
    getOpenAccessColor,
    getOpenAccessOpacity,
}