import { getStyle } from './StyleBaseLib';

/**
 * Get the margin style
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string 
 */
const getMargin = (state = '', val?: string, def?: string) : string => {
    return getStyle(state, false, 'm', val, def);
};
/**
 * Get the margin left/right style
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string 
 */
const getMarginX = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'mx', val, def);
};

/**
 * Get the margin top/bottom style
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string 
 */
const getMarginY = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'my', val, def);
};

/**
 * Get the margin-top style
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string 
 */
const getMarginTop = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'mt', val, def);
};

/**
 * Get the margin-bottom style
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string 
 */
const getMarginBottom = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'mb', val, def);
};

/**
 * Get the margin-left style
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string 
 */
const getMarginLeft = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'ml', val, def);
};

/**
 * Get the margin-right style
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string 
 */
const getMarginRight = (state = '', val?: string, def = '0') : string => {
    return getStyle(state, false, 'mr', val, def);
};

export {
    getMargin,
    getMarginX,
    getMarginY,
    getMarginTop,
    getMarginBottom,
    getMarginRight,
    getMarginLeft,
}