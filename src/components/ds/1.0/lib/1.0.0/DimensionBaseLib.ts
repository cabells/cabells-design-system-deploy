import { getStyle, getAllowedStyle, getAllowedRootStyle } from './StyleBaseLib';
const zIndexConfig = require('../../../../../../design-system-config/1.0.0/zIndex.config');

/**
 * Get width style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 * @returns string
 */
 const getWidth = (state = '', val?: string, def = '') : string => {
    return getStyle(state, false, 'w', val, def);
};

/**
 * Get min-width style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 * @returns string
 */
const getMinWidth = (state = '', val?: string, def = '') : string => {
    return getStyle(state, false, 'min-w', val, def);
};

/**
 * Get max-width style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 * @returns string
 */
const getMaxWidth = (state = '', val?: string, def = '') : string => {
    return getStyle(state, false, 'max-w', val, def);
};

/**
 * Get height style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 * @returns string
 */
const getHeight = (state = '', val?: string, def = '') : string => {
    return getStyle(state, false, 'h', val, def);
};

/**
 * Get min-height style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 * @returns string
 */
const getMinHeight = (state = '', val?: string, def = '') : string => {
    return getStyle(state, false, 'min-h', val, def);
};

/**
 * Get max-height style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 * @returns string
 */
const getMaxHeight = (state = '', val?: string, def = '') : string => {
    return getStyle(state, false, 'max-h', val, def);
};

/**
 * Get display style
 * 
 * @param state string 
 * @param disabled boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
 const getDisplay = (state = '', disabled = false, val?: string, def = '') : string => {
    const allowed = ['block', 'inline-block', 'flex', 'inline-flex', 'grid', 'hidden'];
    return getAllowedRootStyle(state, disabled, allowed, val, def);
};

/**
 * Get visibility style
 * 
 * @param state string 
 * @param disabled boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
 const getVisibility = (state = '', disabled = false, val?: string, def = '') : string => {
    if ('hidden' === val) val = 'invisible'; // alias the original css value "hidden"
    if ('hidden' === def) def = 'invisible';
    const allowed = ['visible', 'invisible'];
    return getAllowedRootStyle(state, disabled, allowed, val, def);
};

/**
 * Get z-index style
 * 
 * @param state string
 * @param disabled boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
const getZIndex = (state = '', disabled = false, val?: string, def = '') : string => {
    const tailwinds = {
        '0' : '0', // to preserve the zeroeth value during concat
        '10' : '10',
        '20' : '20',
        '30' : '30',
        '40' : '40',
        '50' : '50',
        'auto' : 'auto',
    };
    const allowed = Object.keys(zIndexConfig).concat(Object.keys(tailwinds));
    return getAllowedStyle(state, disabled, 'z', allowed, val, def);
};

/**
 * Get overflow style
 * 
 * @param state string 
 * @param disabled boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
 const getOverflow = (state = '', disabled = false, val?: string, def = '') : string => {
    const allowed = ['auto', 'hidden', 'visible', 'scroll'];
    return getAllowedStyle(state, disabled, 'overflow', allowed, val, def);
};

/**
 * Get overflow-x style
 * 
 * @param state string 
 * @param disabled boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
 const getOverflowX = (state = '', disabled = false, val?: string, def = '') : string => {
    const allowed = ['auto', 'hidden', 'visible', 'scroll'];
    return getAllowedStyle(state, disabled, 'overflow-x', allowed, val, def);
};

/**
 * Get overflow-y style
 * 
 * @param state string 
 * @param disabled boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
 const getOverflowY = (state = '', disabled = false, val?: string, def = '') : string => {
    const allowed = ['auto', 'hidden', 'visible', 'scroll'];
    return getAllowedStyle(state, disabled, 'overflow-y', allowed, val, def);
};

/**
 * Get vertical-align style
 * 
 * @param state string 
 * @param disabled boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
 const getVerticalAlign = (state = '', disabled = false, val?: string, def = '') : string => {
    const allowed = ['baseline', 'top', 'middle', 'bottom', 'text-top', 'text-bottom'];
    return getAllowedStyle(state, disabled, 'align', allowed, val, def);
};

export {
    getWidth,
    getMaxWidth,
    getMinWidth,
    getHeight,
    getMaxHeight,
    getMinHeight,
    getDisplay,
    getVisibility,
    getZIndex,
    getOverflow,
    getOverflowX,
    getOverflowY,
    getVerticalAlign,
}