import { getAllowedStyle, getValidColor } from './StyleBaseLib';

/**
 * Get Tailwind background-color style
 * 
 * @param state string
 * @param disabled boolean
 * @param val string|null
 * @param def string|null
 * @returns string
 */
const getBgColor = (state = '', disabled = false, val?: string, def?: string) : string => {
    return getValidColor(state, disabled, val, def, 'bg');
};

/**
 * Get Tailwind box-shadow style
 * 
 * @param state string
 * @param disabled boolean
 * @param val string
 * @param def string Default box shadow value
 * @returns string
 */
const getBoxShadow = (state = '', disabled = false, val: string, def = '-sm') : string => {
    val = val === 'default' ? 'DEFAULT' : val; 
    def = def === 'default' ? 'DEFAULT' : def;
    const boxShadowConfig = require('../../../../../../design-system-config/1.0.0/boxShadow.config');
    const allowed = Object.keys(boxShadowConfig).concat(['none']);
    return getAllowedStyle(state, disabled, 'shadow', allowed, val, def);
};

/**
 * Get Tailwind mix-blend-mode style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 * @returns string
 */
const getMixBlendMode = (state = '', val?: string, def = 'normal') : string => {
    const allowed = ['normal', 'multiply', 'screen', 'overlay', 'darken', 'lighten', 
                     'color-dodge', 'color-burn', 'hard-light', 'soft-light', 'difference',
                     'exclusion', 'hue', 'saturation', 'color', 'luminosity',
    ];
    return getAllowedStyle(state, false, 'mix-blend', allowed, val, def);
};

/**
 * Get Tailwind background-blend-mode style
 * 
 * @param state string
 * @param val string|null
 * @param def string
 * @returns string
 */
const getBgBlendMode = (state = '', val?: string, def = 'normal') : string => {
    const allowed = ['normal', 'multiply', 'screen', 'overlay', 'darken', 'lighten', 
                     'color-dodge', 'color-burn', 'hard-light', 'soft-light', 'difference',
                     'exclusion', 'hue', 'saturation', 'color', 'luminosity',
    ];
    return getAllowedStyle(state, false, 'bg-blend', allowed, val, def);
};

export {
    getBgColor,
    getBoxShadow,
    getMixBlendMode,
    getBgBlendMode,
}