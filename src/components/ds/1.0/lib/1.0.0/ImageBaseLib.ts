const ImgAltDefault = 'Cabells Publishing'; 

/**
 * Get the image alt value,
 * or the default if none is given
 * 
 * @param alt string
 * @returns string
 */
const getImgAlt = (alt = '') : string => {
    if (!alt || alt.length < 0) {
        alt = ImgAltDefault;
    }
    return alt;
};

/**
 * Parse an image dimension value for use
 * in img tag width and height attributes,
 * which conflict with design system properties
 * of the same name. Img tag width/height must
 * be either numbers or percentages, but cannot
 * contain dimension extensions, like px or em.
 * 
 * @param val string
 * @returns string|number
 */
const getImgDim = (val: string|number) : string|number => {
    val = String(val);
    if (val == 'full' || val == '0' || val == '0px' || val == 'auto') {
        val = '100%';
    }
    if (val.endsWith('%')) {
        return val;
    }
    const exts = ['px', 'rem', 'em', 'vw', 'vh'];
    for (const ext in exts) {
        if (val.endsWith(exts[ext])) {
            val = val.replace(exts[ext], '');
        }
    }
    return parseInt(val);
};

export {
    getImgAlt,
    getImgDim,
    ImgAltDefault,
}