import { parseState } from "./StyleBaseLib";

/**
 * Get the Tailwind gap style that is now 
 * available to flex displays as well
 * as grids.
 * 
 * @param state string
 * @param gap string
 * @returns string
 */
 const getGap = (state = '', gap = '') : string => {
    if (gap === '1px') {
        return 'cds-gap-px';
    }
    if (gap.length > 0) {
        return parseState(state) + 'cds-gap-' + gap;
    }
    return '';
};

/**
 * Get the Tailwind gap-x style that is now 
 * available to flex displays as well
 * as grids.
 * 
 * @param state string
 * @param gap string
 * @returns string
 */
 const getGapX = (state = '', gap = '') : string => {
    if (gap === '1px') {
        return 'cds-gap-x-px';
    }
    if (gap.length > 1) {
        return parseState(state) + 'cds-gap-x-' + gap;
    }
    return '';
};

/**
 * Get the Tailwind gap-y style that is now 
 * available to flex displays as well
 * as grids.
 * 
 * @param state string
 * @param gap string
 * @returns string
 */
 const getGapY = (state = '', gap = '') : string => {
    if (gap === '1px') {
        return 'cds-gap-y-px';
    }
    if (gap.length > 0) {
        return parseState(state) + 'cds-gap-y-' + gap;
    }
    return '';
};


/**
 * Get the Tailwind space-x style that is now 
 * available to flex displays as well
 * as grids.
 * 
 * @param state string
 * @param space string
 * @returns string
 */
 const getSpaceX = (state = '', space = '') : string => {
    if (space === '1px') {
        return 'cds-space-x-px';
    }
    if (space.length > 0) {
        return parseState(state) + 'cds-space-x-' + space;
    }
    return '';
};

/**
 * Get the Tailwind space style that is now 
 * available to flex displays as well
 * as grids.
 * 
 * @param state string
 * @param space string
 * @returns string
 */
 const getSpaceY = (state = '', space = '') : string => {
    if (space === '1px') {
        return 'cds-space-y-px';
    }
    if (space.length > 0) {
        return parseState(state) + 'cds-space-y-' + space;
    }
    return '';
};

export {
    getGap,
    getGapX,
    getGapY,
    getSpaceX,
    getSpaceY,
}