import { 
    parseState, 
    getValidColor, 
    isValidStyle, 
    checkDefault,
    getAllowedStyle,
} from './StyleBaseLib';
const ringConfig = require('../../../../../../design-system-config/1.0.0/ringWidth.config');
const ringStyles = Object.keys(ringConfig).concat(['0', '1', '2', '4', '8', 'DEFAULT']);
import { opacityStyles } from './OpacityBaseLib';

/**
 * Get Tailwind ring color style
 * 
 * @param state string
 * @param disabled boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
 const getBorderRingColor = (state = '', disabled = false, val?: string, def = '') : string => {
    return getValidColor(state, disabled, val, def, 'ring');
};

/**
 * Get border-color style
 * 
 * @param state string
 * @param disabled boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
const getBorderColor = (state = '', disabled = false, val?: string, def = '') : string => {
    return getValidColor(state, disabled, val, def, 'border');
};

/**
 * Get border-width style
 * 
 * @param state string
 * @param disable boolean
 * @param val string|null
 * @param def string
 * @param side string
 * @returns string
 */
const getBorderWidth = (state = '', disabled = false, val?: string, def = '', border_side = 'DEFAULT') : string => {
    if (disabled && state != 'disabled') {
        return '';
    }
    const borderWidthConfig = require('../../../../../../design-system-config/1.0.0/borderWidth.config');
    let style = '';
    const sides = ['DEFAULT', 't', 'b', 'l', 'r'];
    const styles = Object.keys(borderWidthConfig).concat(['0']);
    const borderWidth = val ?? def;
    const borderSide = border_side ?? '';
    if (borderWidth && isValidStyle(styles, borderWidth)) {
        if (borderSide && isValidStyle(sides, borderSide)) {
            style += checkDefault(borderSide);
        }
        style += checkDefault(borderWidth);
    }
    return borderWidth ? parseState(state) + 'cds-border' + style : '';
};

/**
 * Get Tailwind border ring style
 * **As this time, ring width styles
 * are broken in production. The style
 * has been reverted to 'border.'
 * 
 * @param state string
 * @param disable boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
const getBorderRingWidth = (state = '', disabled = false, val?: string, def = '') : string => {
    if (disabled && state != 'disabled') {
        return '';
    }
    let style = '';
    const ringWidth = val ?? def;
    if (ringWidth && isValidStyle(ringStyles, ringWidth)) {
        style = checkDefault(ringWidth);
    }
    return ringWidth ? parseState(state) + 'cds-ring' + style : '';
};

/**
 * Get border style 
 * 
 * @param state string
 * @param disabled boolean
 * @param val string|null
 * @param def string
 * @returns string
 */
const getBorderStyle = (state = '', disabled = false, val?: string, def = '') : string => {
    if (disabled && state != 'disabled') {
        return '';
    }
    let style = '-solid';
    const styles = ['solid', 'dotted', 'dashed', 'double', 'none'];
    const borderStyle = val ?? def;
    if (borderStyle && isValidStyle(styles, borderStyle)) {
        style = checkDefault(borderStyle);
    }
    return borderStyle ? parseState(state) + 'cds-border' + style : '';
};

/**
 * Get border-radius style 
 * 
 * @param state string
 * @param disabled boolean
 * @param val string|null
 * @param def string Default border radius value
 * @returns string
 */
const getBorderRadius = (state = '', val?: string, def = '', border_side = 'DEFAULT') : string => {
    const borderRadiusConfig = require('../../../../../../design-system-config/1.0.0/borderRadius.config');
    def = def === 'default' ? 'DEFAULT' : def;
    val = val && val.length > 0 ? val : def;
    let style = '';
    const sides = ['DEFAULT', 't', 'b', 'l', 'r', 'tl', 'tr', 'bl', 'br'];
    const styles = Object.keys(borderRadiusConfig).concat(['none', 'sm', 'md', 'lg', 'xl', 'xxl', 'xxxl']);
    const radius = val ?? def;
    const borderSide = border_side ?? '';
    if (radius && isValidStyle(styles, radius)) {
        if (borderSide && isValidStyle(sides, borderSide)) {
            style += checkDefault(borderSide);
        }
        style += checkDefault(radius);
    }    
    return radius ? parseState(state) + 'cds-rounded' + style : '';
};

/**
 * Get border-opacity style
 * 
 * @param state string
 * @param disabled boolean
 * @param val string
 * @param def string Default border opacity value
 * @returns string
 */
const getBorderOpacity = (state = '', disabled = false, val?: string, def = '') : string => {
    return getAllowedStyle(state, disabled, 'border-opacity', opacityStyles, val, def);
};

export {
    getBorderColor,
    getBorderOpacity,
    getBorderRadius,
    getBorderRingColor,
    getBorderRingWidth,
    getBorderStyle,
    getBorderWidth,
}