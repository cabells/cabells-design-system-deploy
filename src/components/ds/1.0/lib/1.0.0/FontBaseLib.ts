import { getStyle, getAllowedStyle, getValidColor, getAllowedRootStyle } from './StyleBaseLib';

/**
 * Get font-size style 
 * 
 * @param state string
 * @param val string|null
 * @param def string Default font size value
 * @returns string
 */
const getFontSize = (state = '', val?: string, def = '4') : string => {
    return getStyle(state, false, 'text', val, def);
};

/**
 * Get font-weight style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default font weight value
 * @returns string
 */
const getFontWeight = (state = '', val?: string, def = 'normal') : string => {
    const allowed = ['thin', 'extralight', 'light', 'normal', 'medium', 'semibold', 'bold', 'extrabold', 'black'];
    return getAllowedStyle(state, false, 'font', allowed, val, def);
};

/**
 * Get font-family style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default font family value
 * @returns string 
 */
 const getFontFamily = (state = '', val?: string, def = 'sans') : string => {
    const fontFamilyConfig = require('../../../../../../design-system-config/1.0.0/fontFamily.config');
    const allowed = Object.keys(fontFamilyConfig);
    return getAllowedStyle(state, false, 'font', allowed, val, def);
};

/**
 * Get font-style
 * 
 * @param state string
 * @param val boolean|null
 * @param def boolean
 * @returns string
 */
const getItalicStyle = (state = '', val?: boolean, def?: boolean) : string => {
    val = val ?? def;
    if (!val) {
        return '';
    }
    return getAllowedRootStyle(state, false, ['italic'], 'italic');
};

/**
 * Get line-height style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default leading value
 * @returns string
 */
const getLeading = (state = '', val?: string, def = '24') : string => {
    const lineHeightConfig = require('../../../../../../design-system-config/1.0.0/lineHeight.config');
    const allowed = Object.keys(lineHeightConfig);
    allowed.push('none');
    return getAllowedStyle(state, false, 'leading', allowed, val, def);
};

/**
 * Get letter-spacing style
 * 
 * @param state string
 * @param val string|null
 * @param def string Default tracking value
 * @returns string
 */
const getTracking = (state = '', val: string, def = 'sm') : string => {
    const letterSpacingConfig = require('../../../../../../design-system-config/1.0.0/letterSpacing.config');
    const allowed = Object.keys(letterSpacingConfig);
    return getAllowedStyle(state, false, 'tracking', allowed, val, def);
};

/**
 * Get text color style
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string
 */
 const getTextColor = (state = '', val?: string, def?: string) : string => {
    return getValidColor(state, false, val, def, 'text');
};

/**
 * Get text-align style 
 * 
 * @param state string
 * @param val string|null
 * @param def string Default text align value
 * @returns string
 */
 const getTextAlign = (state = '', val?: string, def = 'center') : string => {
    const allowed = ['left', 'right', 'center', 'justify'];
    return getAllowedStyle(state, false, 'text', allowed, val, def);
};

/**
 * Get text-overflow style
 *
 * @param state string
 * @param val string|null
 * @param def string Default text overflow value
 * @returns string
 */
 const getTextOverflow = (state = '', val?: string, def?: string) : string => {
    val = val ?? def;
    if (!val?.length) {
        return '';
    }
    if (val === 'truncate') {
        return 'cds-truncate';
    }
    const allowed = ['ellipsis', 'clip'];
    return getAllowedStyle(state, false, 'overflow', allowed, val, def);
};

/**
 * 
 * Get Tailwind text-decoration style 
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string
 */
const getTextDecoration = (state = '', val?: string, def?: string) : string => {
    const allowed = ['underline', 'no-underline', 'line-through'];
    return getAllowedRootStyle(state, false, allowed, val, def);
};

/**
 * 
 * Get Tailwind text-transform style 
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string
 */
const getTextTransform = (state = '', val?: string, def?: string) : string => {
    const allowed = ['uppercase', 'lowercase', 'capitalize', 'normal-case'];
    return getAllowedRootStyle(state, false, allowed, val, def);
};

/**
 * 
 * Get Tailwind whitespace style 
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string
 */
 const getWhitespace = (state = '', val?: string, def?: string) : string => {
    const allowed = ['normal', 'nowrap', 'pre', 'pre-line', 'pre-wrap'];
    return getAllowedStyle(state, false, 'whitespace', allowed, val, def);
};

/**
 * 
 * Get Tailwind cursor style 
 * 
 * @param state string
 * @param val string|null
 * @param def string|null
 * @returns string
 */
 const getCursor = (state = '', val?: string, def?: string) : string => {
    const allowed = ['auto', 'default', 'pointer', 'wait', 'text', 'move', 'help', 'not-allowed'];
    return getAllowedStyle(state, false, 'cursor', allowed, val, def, false);
};

export {
    getFontSize,
    getFontFamily,
    getFontWeight,
    getItalicStyle,
    getLeading,
    getTracking,
    getTextColor,
    getTextAlign,
    getTextDecoration,
    getTextTransform,
    getTextOverflow,
    getWhitespace,
    getCursor,
}