import Avatar from './Avatar.vue';
import TextAvatar from './TextAvatar.vue';

export {
    Avatar,
    TextAvatar,
}