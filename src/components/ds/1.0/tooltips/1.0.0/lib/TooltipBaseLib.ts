/**
 * Get style indicating which direction the tooltip pointer should face
 * 
 * @param face string
 * @param disabled boolean
 * @returns string
 */
 const getPointerFacing = (face: string, disabled: boolean) : string => {
    if (disabled) {
        face = 'disabled';
    }
    return 'cds-tooltip-face-' + face;
};

/**
 * Get style that ensures pointer bg color is same as tooltip
 * These styles map to css defined in PrimitiveTooltip, and as
 * such, may be composite colors indicating combination of 
 * background and border colors
 * 
 * @param state string
 * @param disabled boolean
 * @param val string
 * @param def string
 * @returns string
 */
const getPointerStyle = (state = '', disabled: boolean, val?: string, def?: string) : string => {
    if (disabled && state != 'disabled') {
        return '';
    }
    val = val ?? def;
    if (val && val.length) {
        return 'cds-tooltip-pointer-' + val;
    }
    return '';
};

export {
    getPointerFacing,
    getPointerStyle,
}