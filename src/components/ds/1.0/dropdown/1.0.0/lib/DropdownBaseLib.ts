import { isCompact } from './MenuItemBase';

type MenuItemObject = {
    component: string,
    compact: boolean,
}

/**
 * 
 * @param menuItems MenuItemObject[]
 * @returns string
 */
const getFocusWidth = (menuItems: MenuItemObject[]) : string => {
    let totalHeight = 0;
    for (const menuItem in menuItems) {
        if (isCompact(menuItems[menuItem].component, menuItems[menuItem].compact)) {
            totalHeight += 24;
        } else {
            totalHeight += 44;
        }
    }
    return (totalHeight > 340) ? '252px' : '264px';
};

export {
    getFocusWidth,
}