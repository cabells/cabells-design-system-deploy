/**
 * List of components that are allowed to be used inside MenuItem components
 * These allowed types must support the *content* property as default slot content.
 * @var string[]
 */
const AllowedMenuItemTypes = ['TextButton', 'TextCompactButton'];

/**
 * Get a valid menu item component
 * 
 * @param component string|null
 * @returns String
 */
const getMenuItemComponent = (component?: string): string => {
    let menuItemComponent = 'TextButton';
    if (component && AllowedMenuItemTypes.includes(component)) {
        menuItemComponent = component;
    }
    return menuItemComponent;
};

/**
 * Get the menu item's content. Defaults to empty string.
 * 
 * @param content string|null
 * @returns string
 */
const getMenuItemContent = (content?: string) : string => {
    let menuItemContent = '';
    if (content && content.length > 0) {
        menuItemContent = content;
    }
    return menuItemContent;
};

/**
 * Get the menu item's height value based on the type
 * of component and/or the compact attribute indication.
 * 
 * @param componentType string
 * @param compactAttr boolean
 * @returns string
 */
const getMenuItemHeight = (componentType: string, compactAttr: boolean) : string => {
    const compact = isCompact(componentType, compactAttr);
    return compact ? '24' : '44';
}

/**
 * Determine if component type is compact
 * 
 * @param componentType string
 * @param compactAttr boolean
 * @returns boolean
 */
 const isCompact = (componentType: string, compactAttr: boolean) : boolean => {
    const compactType = componentType.toLowerCase().indexOf('compact') != -1;
    if (typeof compactAttr == 'undefined') {
        compactAttr = compactType ? true : false;
    }
    // if it's either a compact button component, or the compact attribute is set, it's compact by default
    let isCompact = (compactType || compactAttr) ? true : false;
    if (compactType && !compactAttr) {
        isCompact = false; // but if the compact button component has compact attribute set to false, it's not compact
    }
    return isCompact;
};

export {
    getMenuItemComponent,
    getMenuItemContent,
    getMenuItemHeight,
    isCompact,
}