import DisplayText from './DisplayText.vue';
import DisplaySubhead from './DisplaySubhead.vue';
import Heading from './Heading.vue';
import HeadingSupport from './HeadingSupport.vue';
import MarkedDown from './MarkedDown.vue';
import PreHead from './PreHead.vue';
import PreheadHeading from './PreheadHeading.vue';
import PreheadHeadingSupport from './PreheadHeadingSupport.vue';
import SupportingText from './SupportingText.vue';
import Paragraph from './Paragraph.vue';

export {
    DisplayText,
    DisplaySubhead,
    Heading,
    HeadingSupport,
    MarkedDown,
    PreHead,
    PreheadHeading,
    PreheadHeadingSupport,
    SupportingText,
    Paragraph,
}