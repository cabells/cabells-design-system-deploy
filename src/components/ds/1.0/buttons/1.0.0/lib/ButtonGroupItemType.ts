/**
 * Defines data expected by the ButtonGroupItem component
 * in order to render.
 * 
 * A ButtonGroupItem consists of a button and its 
 * corresponding dropdown, which consists of a list of buttons
 * that might each contain a dropdown as well.
 */
type ButtonGroupItemType = {
    component: string,                      // name of the button component
    item_styles: object,                    // object of design system properties for styling the button component
    content: string,                        // button text
    sub_menu_items?: ButtonGroupItemType,   // data needed to render the buttons in the dropdown (optional)
    disabled?: boolean,                     // button disabled state (optional)
    glyph?: string,                         // button icon name (optional)
    callback?: CallableFunction,            // function to call on button click (optional)
    is_current_page?: Boolean,              // tells the item whether it represents the current page
};

export {
    ButtonGroupItemType,
}