/**
 * Defines data expected by the ButtonTabItem component
 * in order to render.
 * 
 * A ButtonTabItemType consists of a button and its 
 * corresponding active tab indicator, which consists 
 * of a SimpleDivider component
 */
type ButtonTabItemType = {
    item_styles: object,                    // object of design system properties for styling the button component
    content: string,                        // button text
    disabled?: boolean,                     // button disabled state (optional)
    glyph?: string,                         // button icon name (optional)
    callback?: CallableFunction,            // function to call on button click (optional)
    is_current_tab?: Boolean,               // tells the item whether it represents the current tab
};

export {
    ButtonTabItemType,
}