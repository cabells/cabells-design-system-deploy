
/**
 * Set the input's id or name, using the other for default values
 * 
 * @param val string|null
 * @param idname string|null
 * @returns string
 */
const getInputNameId = (val?: string, idname?: string) : string => {
    const id = val ?? idname;
    return id ?? '';
};

export {
    getInputNameId,
}