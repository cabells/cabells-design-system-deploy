import { parseState } from "../../../lib/1.0.0/StyleBaseLib";

/**
 * Get the Tailwind grid-row-start style
 * 
 * @param state string
 * @param val string|null
 * @returns string
 */
const getRowStart = (state = '', val?: string) : string => {
    if (!val || val.length < 1) {
        return '';
    }
    return parseState(state) + 'cds-row-start-' + val;
};

/**
 * Get the Tailwind grid-col-start style
 * 
 * @param state string
 * @param val string|null
 * @returns string
 */
const getColStart = (state = '', val?: string) : string => {
    if (!val || val.length < 1) {
        return '';
    }
    return parseState(state) + 'cds-col-start-' + val;
};

/**
 * Get the Tailwind grid-row-span style
 * 
 * @param state string
 * @param val string|null
 * @returns string
 */
const getRowSpan = (state = '', val?: string) : string => {
    if (!val || val.length < 1) {
        return '';
    }
    return parseState(state) + 'cds-row-span-' + val;
};

/**
 * Get the Tailwind grid-col-span style
 * 
 * @param state string
 * @param val string|null
 * @returns string
 */
const getColSpan = (state = '', val?: string) : string => {
    if (!val || val.length < 1) {
        return '';
    }
    return parseState(state) + 'cds-col-span-' + val;
};

export {
    getRowStart,
    getRowSpan,
    getColStart,
    getColSpan,
}