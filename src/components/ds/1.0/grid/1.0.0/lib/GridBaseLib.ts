import { parseState } from '../../../lib/1.0.0/StyleBaseLib';

/**
 * 
 * @param state string
 * @param val string|null
 * @returns string
 */
const getGridLayoutCols = (state = '', val?: String) : string => {
    if (!val || val.length < 1) {
        return '';
    }
    return parseState(state) + 'cds-grid-cols-' + val;
};

/**
 * 
 * @param state string
 * @param val string|null
 * @returns string
 */
const getGridLayoutRows = (state = '', val?: String) : string => {
    if (!val || val.length < 1) {
        return '';
    }
    return parseState(state) + 'cds-grid-rows-' + val;
};

export {
    getGridLayoutCols,
    getGridLayoutRows,
}