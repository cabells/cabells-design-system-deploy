import PrimitiveTag from './PrimitiveTag.vue';
import PrimitiveCompactTag from './PrimitiveCompactTag.vue';
import PrimaryTag from './PrimaryTag.vue';
import PrimaryCompactTag from './PrimaryCompactTag.vue';
import PredatoryTag from './PredatoryTag.vue';
import PredatoryCompactTag from './PredatoryCompactTag.vue';
import PredatoryWarningTag from './PredatoryWarningTag.vue';
import PredatoryWarningCompactTag from './PredatoryWarningCompactTag.vue';
import SecondaryTag from './SecondaryTag.vue';
import SecondaryCompactTag from './SecondaryCompactTag.vue';
import TagBase from './lib/TagBase.vue';
import SecondaryTagBase from './lib/SecondaryTagBase.vue';
import IconTag from './IconTag.vue';

export {
    PrimitiveTag,
    PrimitiveCompactTag,
    PrimaryTag,
    PrimaryCompactTag,
    PredatoryTag,
    PredatoryCompactTag,
    PredatoryWarningTag,
    PredatoryWarningCompactTag,
    SecondaryTag,
    SecondaryCompactTag,
    TagBase,
    SecondaryTagBase,
    IconTag,
}