/**
 * Get various border values determined by whether 
 * this status tag is inactive
 * 
 * @param style_value string|null
 * @param inactive_style_value string|null
 * @param def string
 * @param definactive string
 * @param inactive boolean
 * @returns string
 */
const getValueByActive = (
    style_value?: string, 
    inactive_style_value?: string, 
    def = '', 
    definactive = '', 
    inactive = false
) : string => {
    const styleValue = inactive ? inactive_style_value : style_value;
    const styleValueDefault = inactive ? definactive : def;
    const styleVal = styleValue ?? styleValueDefault;
    return (styleVal && styleVal.length > 1) ? styleVal : '';
};

export {
    getValueByActive,
}