const { DateTime } = require('luxon');

const DefaultTimezoneIANA = 'America/Chicago'; // CST/CDT

/**
 * Maps to a Luxon object (DateTime)
 * 
 * @param type OffsetTypes timezone type [Narrow, Short, Techie]
 */
interface OffsetOptions {
    type: OffsetTypes
}

/**
 * List of Timezone offset types supported by Luxon
 */
enum OffsetTypes {
    Narrow, // +5
    Short, // +05:00
    Techie, // +0500
}

/**
 * Get a DateTime object from SQL datetimes,
 * using optional IANA timezone to establish 
 * time reference. Default IANA value is America/Chicago.
 * 
 * @param date string SQL datetime
 * @param iana string|null IANA timezone
 */
const _getZonedTime = (date: string, iana?: string) : typeof DateTime => {
    if (!iana || iana.length < 1) {
        iana = DefaultTimezoneIANA;
    }
    return DateTime.fromSQL(date, {zone: iana});
};

/**
 * Get custom UTC offset text to append to formatted datetimes
 * 
 * @param datetime DateTime
 * @param options OffsetOptions|null
 */
const _withOffset = (datetime: typeof DateTime, options?: OffsetOptions) : string => {
    let format = datetime.toFormat('Z');
    if (options) {
        switch (options.type) {
            case OffsetTypes.Techie: {
                format = datetime.toFormat('ZZZ');
                break;
            }
            case OffsetTypes.Short: {
                format = datetime.toFormat('ZZ');
                break;
            }
            case OffsetTypes.Narrow:
            default: {
                format = datetime.toFormat('Z');
                break;
            }
        }
    }
    return ' UTC ' + format;
};

/**
 * Format an SQL datetime value to a simple time of day
 * [4:00 PM]
 * 
 * @param time string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatTime = (time?: string, iana?: string) : string => {
    if (time == null || time.length < 1) {
        return '-';
    }
    return _getZonedTime(time, iana).toLocaleString(DateTime.TIME_SIMPLE);
};

/**
 * Format an SQL datetime value to a 24-hour time of day
 * [16:00]
 * 
 * @param time string SQL datetime
 * @param iana string|null IANA timezone
 */
const format24HTime = (time?: string, iana?: string) : string => {
    if (time == null || time.length < 1) {
        return '-';
    }
    return _getZonedTime(time, iana).toLocaleString(DateTime.TIME_24_SIMPLE);
};

/**
 * Format an SQL datetime value to an absolute time
 * [03 Aug 2020 3:30 PM UTC -5]
 * 
 * @param time string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatAbsTime = (time: string, iana?: string) : string => {
    const zonedTime = _getZonedTime(time, iana);
    const date = zonedTime.toFormat('dd MMM yyyy tt');
    return date + _withOffset(zonedTime);
};

/**
 * Format an SQL datetime value to a relative value (now)
 * [a year ago, in three weeks]
 * 
 * @param time string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatRelTime = (time: string, iana?: string) : string => {
    return _getZonedTime(time, iana).toRelative();
};

/**
 * Format an SQL datetime value to a relative value (today)
 * [tomorrow, yesterday]
 * 
 * @param date string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatRelDate = (date: string, iana?: string) : string => {
    return _getZonedTime(date, iana).toRelativeCalendar();
};

/**
 * Format an SQL datetime value to day month year
 * 
 * @param date string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatMonthDayYear = (date: string, iana?: string) : string => {
    return _getZonedTime(date, iana).toFormat('dd MMM yyyy');
};

/**
 * Format an SQL datetime value for times in the distant past
 * 
 * @param date string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatDistantPast = (date: string, iana?: string) : string => {
    return _getZonedTime(date, iana).toFormat('MMM dd, yyyy');
};

/**
 * Format an SQL datetime value with the weekday
 * 
 * @param date string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatWeekday = (date: string, iana?: string) : string => {
    const zonedTime = _getZonedTime(date, iana);
    const weekday = zonedTime.toFormat('ccc, MMM, d, H:mm a');
    return weekday + _withOffset(zonedTime);
};

/**
 * Format an SQL datetime value to an abbreviated month
 * 
 * @param date string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatAbbrevMonth = (date: string, iana?: string) : string => {
    return _getZonedTime(date, iana).toFormat('MMM d');
};

/**
 * Format an SQL datetime value to an abbreviated date 
 * 
 * @param date string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatAbbrevDate = (date: string, iana?: string) : string => {
    return _getZonedTime(date, iana).toFormat('yyyy-MM-dd');
};

/**
 * Format an SQL datetime value to an abbreviated day 
 * 
 * @param date string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatAbbrevDay = (date: string, iana?: string) : string => {
    return _getZonedTime(date, iana).toFormat('EEE');
};

/**
 * Format an SQL datetime value to an abbreviated day and time 
 * 
 * @param date string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatAbbrevDayTime = (date: string, iana?: string) : string => {
    return _getZonedTime(date, iana).toFormat('EEE, t');
};

/**
 * Format an SQL datetime value to an abbreviated 24-hour day and time 
 * 
 * @param date string SQL datetime
 * @param iana string|null IANA timezone
 */
const formatAbbrev24HDayTime = (date: string, iana?: string) : string => {
    return _getZonedTime(date, iana).toFormat('EEE, T');
};

export { 
    formatTime, 
    format24HTime,
    formatAbsTime, 
    formatRelTime, 
    formatRelDate, 
    formatMonthDayYear, 
    formatDistantPast,
    formatWeekday,
    formatAbbrevMonth,
    formatAbbrevDate,
    formatAbbrevDay,
    formatAbbrevDayTime,
    formatAbbrev24HDayTime,
};