
/**
 * Defines data expected by the HeatmapGridDisciplineType's 
 * CabellsScore and Topics.CabellsScore defined Types.
 * 
 * Preview boolean indicates which type of heatmap to render: preview or regular
 * 
 */
type HeatmapScoreType = {
    Year: string,
    Score: string|number,
    AdjustedSnapshotScore?: string|number,
};

/**
 * Defines data expected by the HeatmapGridType's
 * Disciplines property
 */
type HeatmapGridDisciplineType = {
    DisciplineName: string,
    Scores: HeatmapScoreType,
    Topics?: {
        TopicName: string,
        Scores: HeatmapScoreType,
    }, 
};

/**
 * Defines data expected by the PrimitiveHeatmap component
 * in order to render.
 * 
 * A PrimitiveHeatmap consists of a Grid component and a matrix of  
 * GridCells. Each cell has the same background color, while getting 
 * a discrete opacity value from the HeatmapGridType.
 */
type HeatmapGridType = {
    Disciplines: HeatmapGridDisciplineType,
    IsPreview: boolean,
};

export {
    HeatmapScoreType,
    HeatmapGridType,
    HeatmapGridDisciplineType,
}
