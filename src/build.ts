// This file is used to build the design system as a library

const dsComponents: Array<any> = [];

/* Anchors */
import Anchor from './components/ds/1.0/anchors/1.0.0/Anchor.vue';
dsComponents.push(Anchor);

/* Avatars */
import {
    Avatar,
    TextAvatar,
} from './components/ds/1.0/avatars/1.0.0/';
dsComponents.push(Avatar, TextAvatar);

/* Buttons */
import ButtonBase from './components/ds/1.0/buttons/1.0.0/lib/ButtonBase.vue';
import PrimitiveButton from './components/ds/1.0/buttons/1.0.0/primitive/PrimitiveButton.vue';
import PrimitiveCompactButton from './components/ds/1.0/buttons/1.0.0/primitive/PrimitiveCompactButton.vue';
import PrimaryButton from './components/ds/1.0/buttons/1.0.0/primary/PrimaryButton.vue';
import PrimaryCompactButton from './components/ds/1.0/buttons/1.0.0/primary/PrimaryCompactButton.vue';
import SecondaryButton from './components/ds/1.0/buttons/1.0.0/secondary/SecondaryButton.vue';
import SecondaryCompactButton from './components/ds/1.0/buttons/1.0.0/secondary/SecondaryCompactButton.vue';
import TextButton from './components/ds/1.0/buttons/1.0.0/text/TextButton.vue';
import TextCompactButton from './components/ds/1.0/buttons/1.0.0/text/TextCompactButton.vue';
import TextButtonSmall from './components/ds/1.0/buttons/1.0.0/text/TextButtonSmall.vue';
import HeroButton from './components/ds/1.0/buttons/1.0.0/hero/HeroButton.vue';
import HeroOutlineButton from './components/ds/1.0/buttons/1.0.0/hero/HeroOutlineButton.vue';
import ModalButton from './components/ds/1.0/buttons/1.0.0/modal/ModalButton.vue';
import ModalOutlineButton from './components/ds/1.0/buttons/1.0.0/modal/ModalOutlineButton.vue';
import ButtonGroup from './components/ds/1.0/buttons/1.0.0/group/ButtonGroup.vue';
import ButtonGroupItem from './components/ds/1.0/buttons/1.0.0/group/ButtonGroupItem.vue';
import ButtonTabGroup from './components/ds/1.0/buttons/1.0.0/group/ButtonTabGroup.vue';
import ButtonTabItem from './components/ds/1.0/buttons/1.0.0/group/ButtonTabItem.vue';
import KabobGroup from './components/ds/1.0/buttons/1.0.0/group/KabobGroup.vue';
import SegmentedControl from './components/ds/1.0/buttons/1.0.0/group/SegmentedControl.vue';
import ButtonSegment from './components/ds/1.0/buttons/1.0.0/group/ButtonSegment.vue';
import NavsoButton from './components/ds/1.0/buttons/1.0.0/navso/NavsoButton.vue';
dsComponents.push(
    ButtonBase, 
    PrimitiveButton, 
    PrimitiveCompactButton, 
    PrimaryButton, 
    PrimaryCompactButton,
    SecondaryButton, 
    SecondaryCompactButton,
    TextButton,
    TextCompactButton,
    TextButtonSmall,
    HeroButton,
    HeroOutlineButton,
    ModalButton,
    ModalOutlineButton,
    ButtonGroup,
    ButtonGroupItem,
    ButtonTabGroup,
    ButtonTabItem,
    KabobGroup,
    NavsoButton,
    SegmentedControl,
    ButtonSegment,
);

/* Cards */
import CardBase from './components/ds/1.0/cards/1.0.0/lib/CardBase.vue';
import JournalCardBase from './components/ds/1.0/cards/1.0.0/lib/JournalCardBase.vue';
import JournalCardExpandBase from './components/ds/1.0/cards/1.0.0/lib/JournalCardExpandBase.vue';
import PrimitiveCard from './components/ds/1.0/cards/1.0.0/PrimitiveCard.vue';
import PredatoryJournalCard from './components/ds/1.0/cards/1.0.0/PredatoryJournalCard.vue';
import PredatoryJournalCardSmall from './components/ds/1.0/cards/1.0.0/PredatoryJournalCardSmall.vue';
import JournalCard from './components/ds/1.0/cards/1.0.0/JournalCard.vue';
import JournalCardSmall from './components/ds/1.0/cards/1.0.0/JournalCardSmall.vue';
import JournalCardNav from './components/ds/1.0/cards/1.0.0/JournalCardNav.vue';
import PredatoryJournalCardNav from './components/ds/1.0/cards/1.0.0/PredatoryJournalCardNav.vue';
import JournalCardKabobMenu from './components/ds/1.0/cards/1.0.0/JournalCardKabobMenu.vue';
import JournalCardExpand from './components/ds/1.0/cards/1.0.0/JournalCardExpand.vue';
import PredatoryJournalCardExpand from './components/ds/1.0/cards/1.0.0/PredatoryJournalCardExpand.vue';

import PrimitiveJournalCardSection from './components/ds/1.0/cards/1.0.0/sections/PrimitiveJournalCardSection.vue';

import CciSnapshot from './components/ds/1.0/cards/1.0.0/snapshots/CciSnapshot.vue';
import KpiSnapshot from './components/ds/1.0/cards/1.0.0/snapshots/KpiSnapshot.vue';
import MetricSnapshot from './components/ds/1.0/cards/1.0.0/snapshots/MetricSnapshot.vue';
import PrimitiveSnapshot from './components/ds/1.0/cards/1.0.0/snapshots/PrimitiveSnapshot.vue';
import PredatorySnapshotLarge from './components/ds/1.0/cards/1.0.0/snapshots/PredatorySnapshotLarge.vue';
import PredatoryOverviewStripe from './components/ds/1.0/cards/1.0.0/snapshots/PredatoryOverviewStripe.vue';

import PercentileBadge from './components/ds/1.0/cards/1.0.0/badges/PercentileBadge.vue';

import DashboardSegment from './components/ds/1.0/segments/DashboardSegment.vue';
import JournalStatSegment from './components/ds/1.0/segments/JournalStatSegment.vue';
import JournalStatGrouping from './components/ds/1.0/segments/JournalStatGrouping.vue';
import JournalStatRowGrouping from './components/ds/1.0/segments/JournalStatRowGrouping.vue';
import JournalStatAnchorGrouping from './components/ds/1.0/segments/JournalStatAnchorGrouping.vue';
import IconSegmentRight from './components/ds/1.0/segments/IconSegmentRight.vue';

dsComponents.push(
    CardBase, 
    JournalCardBase,
    JournalCardExpandBase,
    PrimitiveCard,
    JournalCard,
    JournalCardSmall,
    JournalCardNav,
	PredatoryJournalCardNav,
    JournalCardKabobMenu,
    PredatoryJournalCard,
    PredatoryJournalCardSmall,
    JournalCardExpand,
    PredatoryJournalCardExpand,
    PrimitiveJournalCardSection,
    CciSnapshot,
    KpiSnapshot,
    MetricSnapshot,
    PrimitiveSnapshot,
	PredatorySnapshotLarge,
	PredatoryOverviewStripe,
    PercentileBadge,
    DashboardSegment,
    JournalStatSegment,
    JournalStatGrouping,
    JournalStatRowGrouping,
    JournalStatAnchorGrouping,
    IconSegmentRight
);

/* Charts */
import {
    DonutChart,
} from './components/ds/1.0/charts/1.0.0/';
dsComponents.push(DonutChart);

/* Data */
import SimpleTime from './components/ds/1.0/data/1.0.0/SimpleTime.vue';
import Simple24HTime from './components/ds/1.0/data/1.0.0/Simple24HTime.vue';
import MonthDayYear from './components/ds/1.0/data/1.0.0/MonthDayYear.vue';
import AbsTime from './components/ds/1.0/data/1.0.0/AbsTime.vue';
import RelTime from './components/ds/1.0/data/1.0.0/RelTime.vue';
import RelDate from './components/ds/1.0/data/1.0.0/RelDate.vue';
import DistantPast from './components/ds/1.0/data/1.0.0/DistantPast.vue';
import Weekday from './components/ds/1.0/data/1.0.0/Weekday.vue';
import ShortMonth from './components/ds/1.0/data/1.0.0/ShortMonth.vue';
import ShortDate from './components/ds/1.0/data/1.0.0/ShortDate.vue';
import ShortDay from './components/ds/1.0/data/1.0.0/ShortDay.vue';
import ShortDayTime from './components/ds/1.0/data/1.0.0/ShortDayTime.vue';
import Short24HDayTime from './components/ds/1.0/data/1.0.0/Short24HDayTime.vue';
import ShortText from './components/ds/1.0/data/1.0.0/ShortText.vue';

import HeatmapBase from './components/ds/1.0/data/1.0.0/heatmaps/lib/HeatmapBase.vue';
import Heatmap from './components/ds/1.0/data/1.0.0/heatmaps/Heatmap.vue';
import HeatmapCell from './components/ds/1.0/data/1.0.0/heatmaps/HeatmapCell.vue';
import HeatmapPreview from './components/ds/1.0/data/1.0.0/heatmaps/HeatmapPreview.vue';
import PrimitiveHeatmap from './components/ds/1.0/data/1.0.0/heatmaps/PrimitiveHeatmap.vue';
import CardSectionHeatmap from './components/ds/1.0/data/1.0.0/heatmaps/CardSectionHeatmap.vue';

import {
    formatTime, 
    format24HTime,
    formatAbsTime, 
    formatRelTime, 
    formatRelDate, 
    formatMonthDayYear, 
    formatAbbrevMonth,
    formatAbbrevDate,
    formatAbbrevDay,
    formatAbbrevDayTime,
    formatAbbrev24HDayTime,
} from './components/ds/1.0/data/1.0.0/Format';
import {
    HeatmapGridType,
    HeatmapScoreType,
    HeatmapGridDisciplineType,
} from './components/ds/1.0/data/1.0.0/heatmaps/lib/HeatmapGridType';
dsComponents.push(
    SimpleTime, 
    Simple24HTime, 
    MonthDayYear, 
    AbsTime, 
    RelTime,
    RelDate,
    DistantPast, 
    Weekday,
    ShortMonth,
    ShortDate,
    ShortDay,
    ShortDayTime,
    Short24HDayTime,
    ShortText,
    HeatmapBase,
    Heatmap,
    HeatmapCell,
    HeatmapPreview,
    PrimitiveHeatmap,
    CardSectionHeatmap
);

/* Dividers */
import { 
    SimpleDivider, 
    VerticalDivider, 
    TitleDivider, 
    CaptionDivider,
    MenuItemDivider,
} 
from './components/ds/1.0/dividers/1.0.0/';
dsComponents.push(
    SimpleDivider, 
    VerticalDivider, 
    TitleDivider, 
    CaptionDivider,
    MenuItemDivider
);

/* Dropdowns */
import Dropdown from './components/ds/1.0/dropdown/1.0.0/Dropdown.vue';
import MenuItem from './components/ds/1.0/dropdown/1.0.0/MenuItem.vue';
dsComponents.push(Dropdown, MenuItem);

/* Flex */
import { 
    FlexCore, 
    FlexStretch, 
    FlexCenter, 
    FlexTopLeft,
    FlexCell,
    FlexCellBase,
    Flexbox,
    FlexboxInline,
    FlexGridFull,
    FlexGridHalf,
    FlexGrid,
    FlexGridLayout,
    FlexGridThird,
    FlexGridTwoThirds,
    FlexGridFourth,
    FlexGridThreeFourths,
    FlexGridFifth,
    FlexGridTwoFifths,
    FlexGridThreeFifths,
    FlexGridFourFifths,
    FlexGridSixth,
    FlexGridFiveSixths,
} from './components/ds/1.0/flex/1.0.0/';
import { 
    getOrder, 
    getFlexAnimatable,
    getFlexWidth,
    getFlexStyle,
    getJustifyStyle,
    getAlignStyle,
    getAllowedList,
    getSmScreen,
    getDefScreen,
} from './components/ds/1.0/flex/1.0.0/lib/FlexCellLib';
dsComponents.push(
    FlexCore, 
    FlexStretch, 
    FlexCenter, 
    FlexTopLeft,
    FlexCell,
    Flexbox,
    FlexboxInline,
    FlexGridFull,
    FlexGridHalf,
    FlexGrid,
    FlexGridLayout,
    FlexGridThird,
    FlexGridTwoThirds,
    FlexGridFourth,
    FlexGridThreeFourths,
    FlexGridFifth,
    FlexGridTwoFifths,
    FlexGridThreeFifths,
    FlexGridFourFifths,
    FlexGridSixth,
    FlexGridFiveSixths
);

/* Grid */
import { Grid as CdsGrid, GridCell } from './components/ds/1.0/grid/1.0.0/';
dsComponents.push(CdsGrid, GridCell);

/* Icons */
import IconBase from './components/ds/1.0/icons/1.0.0/lib/IconBase.vue';
import PrimitiveIcon from './components/ds/1.0/icons/1.0.0/PrimitiveIcon.vue';
import Icon from './components/ds/1.0/icons/1.0.0/Icon.vue';
import IconXs from './components/ds/1.0/icons/1.0.0/IconXs.vue';
import IconXl from './components/ds/1.0/icons/1.0.0/IconXl.vue';
import Icon4 from './components/ds/1.0/icons/1.0.0/Icon4.vue';
import Icon6 from './components/ds/1.0/icons/1.0.0/Icon6.vue';
import Icon8 from './components/ds/1.0/icons/1.0.0/Icon8.vue';
import IconFontText from './components/ds/1.0/icons/1.0.0/IconFontText.vue';
import IconText from './components/ds/1.0/icons/1.0.0/IconText.vue';
import IconTooltip from './components/ds/1.0/icons/1.0.0/IconTooltip.vue';
import PredatoryIcon from './components/ds/1.0/icons/1.0.0/PredatoryIcon.vue';
dsComponents.push(
    IconBase, 
    PrimitiveIcon,
    PredatoryIcon,
    Icon, 
    IconXs,
    IconXl,
    Icon4,
    Icon6,
    Icon8,
    IconText,
    IconFontText,
    IconTooltip
);
import {
    getIconClass,
    getIconCharFromCode,
} from './components/ds/1.0/icons/1.0.0/lib/IconBaseLib';

/* Images */
import ImgBase from './components/ds/1.0/images/1.0.0/lib/ImgBase.vue';
import PrimitiveImage from './components/ds/1.0/images/1.0.0/PrimitiveImage.vue';
import ImageGradient from './components/ds/1.0/images/1.0.0/ImageGradient.vue';

// logo svgs
import AltmetricsLogo from './components/ds/1.0/images/1.0.0/svg/AltmetricsLogo.vue';
import SciteLogo from './components/ds/1.0/images/1.0.0/svg/SciteLogo.vue';
import CabellsLogo from './components/ds/1.0/images/1.0.0/svg/CabellsLogo.vue';

import GreenCheckCircle from './components/ds/1.0/images/1.0.0/svg/GreenCheckCircle.vue';
import BanCircle from './components/ds/1.0/images/1.0.0/svg/BanCircle.vue';
import HelpCircle from './components/ds/1.0/images/1.0.0/svg/HelpCircle.vue';

// svg badges
import GreenOaBadge from './components/ds/1.0/images/1.0.0/svg/GreenOaBadge.vue';
import GreenOaBadgeSmall from './components/ds/1.0/images/1.0.0/svg/GreenOaBadgeSmall.vue';

dsComponents.push(
    ImgBase, PrimitiveImage, ImageGradient, 
    AltmetricsLogo, SciteLogo, CabellsLogo,
    GreenCheckCircle, BanCircle, HelpCircle,
    GreenOaBadge, GreenOaBadgeSmall
);

/* Inputs */
import PrimitiveToggleInput from './components/ds/1.0/inputs/1.0.0/PrimitiveToggleInput.vue';
import PrimitiveTextInput from './components/ds/1.0/inputs/1.0.0/PrimitiveTextInput.vue';
import PrimitiveInputLabel from './components/ds/1.0/inputs/1.0.0/PrimitiveInputLabel.vue';
import TextInput from './components/ds/1.0/inputs/1.0.0/TextInput.vue';
import TextInputBase from './components/ds/1.0/inputs/1.0.0/lib/TextInputBase.vue';
import InputLabelBase from './components/ds/1.0/inputs/1.0.0/lib/InputLabelBase.vue';
import ToggleInputBase from './components/ds/1.0/inputs/1.0.0/lib/ToggleInputBase.vue';
import PrimitiveSelectInput from './components/ds/1.0/inputs/1.0.0/PrimitiveSelectInput.vue';
import SelectInput from './components/ds/1.0/inputs/1.0.0/SelectInput.vue';
import SelectTextInput from './components/ds/1.0/inputs/1.0.0/SelectTextInput.vue';
import SelectInputBase from './components/ds/1.0/inputs/1.0.0/lib/SelectInputBase.vue';

dsComponents.push(
    PrimitiveToggleInput, 
    PrimitiveTextInput, 
    PrimitiveInputLabel, 
    TextInput,
    TextInputBase,
    InputLabelBase,
    ToggleInputBase,
    PrimitiveSelectInput,
    SelectInput,
	SelectTextInput,
    SelectInputBase
);

/* Lib */
import ComponentBase from './components/ds/1.0/lib/1.0.0/ComponentBase.vue';
import BgBase from './components/ds/1.0/lib/1.0.0/BgBase.vue';
import BorderBase from './components/ds/1.0/lib/1.0.0/BorderBase.vue';
import DimensionBase from './components/ds/1.0/lib/1.0.0/DimensionBase.vue';
import FlexBase from './components/ds/1.0/lib/1.0.0/FlexBase.vue';
import FontBase from './components/ds/1.0/lib/1.0.0/FontBase.vue';
import HoverBase from './components/ds/1.0/lib/1.0.0/HoverBase.vue';
import ToggleBase from './components/ds/1.0/lib/1.0.0/ToggleBase.vue';
import ImageBase from './components/ds/1.0/lib/1.0.0/ImageBase.vue';
import MarginBase from './components/ds/1.0/lib/1.0.0/MarginBase.vue';
import PaddingBase from './components/ds/1.0/lib/1.0.0/PaddingBase.vue';
import PositionBase from './components/ds/1.0/lib/1.0.0/PositionBase.vue';
import OpacityBase from './components/ds/1.0/lib/1.0.0/OpacityBase.vue';
import SelectableBase from './components/ds/1.0/lib/1.0.0/SelectableBase.vue';
import StyleBase from './components/ds/1.0/lib/1.0.0/StyleBase.vue';
import UtilityBase from './components/ds/1.0/lib/1.0.0/UtilityBase.vue';
import PrimitiveDiv from './components/ds/1.0/lib/1.0.0/PrimitiveDiv.vue';
import ProgressIndicator from './components/ds/1.0/lib/1.0.0/ProgressIndicator.vue';
dsComponents.push(
    ComponentBase, 
    BgBase, 
    BorderBase, 
    DimensionBase,
    FlexBase,
    FontBase,
    HoverBase,
    ToggleBase,
    ImageBase,
    MarginBase,
    PaddingBase,
    PositionBase,
    OpacityBase,
    SelectableBase,
    StyleBase,
    UtilityBase,
    PrimitiveDiv,
    ProgressIndicator
);
import {
    getBgColor,
    getBoxShadow,
    getMixBlendMode,
    getBgBlendMode,
} from './components/ds/1.0/lib/1.0.0/BgBaseLib';
import {
    getBorderColor,
    getBorderOpacity,
    getBorderRadius,
    getBorderRingColor,
    getBorderRingWidth,
    getBorderStyle,
    getBorderWidth,
} from './components/ds/1.0/lib/1.0.0/BorderBaseLib';
import {
    getWidth,
    getMinWidth,
    getMaxWidth,
    getHeight,
    getMinHeight,
    getMaxHeight,
    getDisplay,
    getVisibility,
    getZIndex,
    getOverflow,
    getOverflowX,
    getOverflowY,
    getVerticalAlign,
} from './components/ds/1.0/lib/1.0.0/DimensionBaseLib';
import {
    getItemAlign,
    getFlexDirection,
    getFlexWrap,
} from './components/ds/1.0/lib/1.0.0/FlexBaseLib';
import {
    getFontSize,
    getFontFamily,
    getFontWeight,
    getItalicStyle,
    getLeading,
    getTracking,
    getTextColor,
    getTextAlign,
    getTextDecoration,
    getTextTransform,
    getWhitespace,
    getCursor,
} from './components/ds/1.0/lib/1.0.0/FontBaseLib';
import {
    getImgAlt,
    getImgDim,
    ImgAltDefault,
} from './components/ds/1.0/lib/1.0.0/ImageBaseLib';
import {
    getMargin, getMarginX, getMarginY,
    getMarginTop, getMarginBottom,
    getMarginRight, getMarginLeft,
} from './components/ds/1.0/lib/1.0.0/MarginBaseLib';
import {
    getOpacity,
    getBgOpacity,
    getTextOpacity,
    getDivideOpacity,
    getRingOpacity, 
} from './components/ds/1.0/lib/1.0.0/OpacityBaseLib';
import {
    getPadding,
    getPaddingTop,
    getPaddingBottom,
    getPaddingLeft,
    getPaddingRight,
    getPaddingX,
    getPaddingY,  
} from './components/ds/1.0/lib/1.0.0/PaddingBaseLib';
import {
    getPosition,
    getTop,
    getBottom,
    getLeft,
    getRight,
} from './components/ds/1.0/lib/1.0.0/PositionBaseLib';
import {
    getGap,
    getGapX,
    getGapY,
    getSpaceX,
    getSpaceY,
} from './components/ds/1.0/lib/1.0.0/SpacingBaseLib';
import {
    checkDefault,
    isValidStyle,
    parseState,
    getValidColor,
    getStyle,
    getAllowedStyle,
    getAllowedRootStyle,
    getOpenAccessColor,
    getOpenAccessOpacity,
} from './components/ds/1.0/lib/1.0.0/StyleBaseLib';

/* Pills */
import PillBase from './components/ds/1.0/pills/1.0.0/lib/PillBase.vue';
import PrimitivePill from './components/ds/1.0/pills/1.0.0/PrimitivePill.vue';
import FilterPill from './components/ds/1.0/pills/1.0.0/FilterPill.vue';
dsComponents.push(
    PillBase,
    PrimitivePill,
    FilterPill
);

/* Tags */
import PrimitiveTag from './components/ds/1.0/tags/1.0.0/PrimitiveTag.vue';
import PrimitiveCompactTag from './components/ds/1.0/tags/1.0.0/PrimitiveCompactTag.vue';
import PrimaryTag from './components/ds/1.0/tags/1.0.0/PrimaryTag.vue';
import PrimaryCompactTag from './components/ds/1.0/tags/1.0.0/PrimaryCompactTag.vue';
import PredatoryTag from './components/ds/1.0/tags/1.0.0/PredatoryTag.vue';
import PredatoryCompactTag from './components/ds/1.0/tags/1.0.0/PredatoryCompactTag.vue';
import PredatoryWarningTag from './components/ds/1.0/tags/1.0.0/PredatoryWarningTag.vue';
import PredatoryWarningCompactTag from './components/ds/1.0/tags/1.0.0/PredatoryWarningCompactTag.vue';
import SecondaryTag from './components/ds/1.0/tags/1.0.0/SecondaryTag.vue';
import SecondaryCompactTag from './components/ds/1.0/tags/1.0.0/SecondaryCompactTag.vue';
import IconTag from './components/ds/1.0/tags/1.0.0/IconTag.vue';
import SecondaryTagBase from './components/ds/1.0/tags/1.0.0/lib/SecondaryTagBase.vue';
import TagBase from './components/ds/1.0/tags/1.0.0/lib/TagBase.vue';
dsComponents.push(
    PrimitiveTag, 
    PrimitiveCompactTag, 
    PrimaryTag, 
    PrimaryCompactTag,
    PredatoryTag,
    PredatoryCompactTag,
    PredatoryWarningTag,
    PredatoryWarningCompactTag,
    SecondaryTag,
    SecondaryCompactTag,
    IconTag,
    SecondaryTagBase,
    TagBase
);
import {
    getValueByActive, 
} from './components/ds/1.0/tags/1.0.0/lib/SecondaryTagLib';

/* Tooltips */
import PrimitiveTooltip from './components/ds/1.0/tooltips/1.0.0/PrimitiveTooltip.vue';
import TooltipIconLabel from './components/ds/1.0/tooltips/1.0.0/TooltipIconLabel.vue';
import TooltipHelpText from './components/ds/1.0/tooltips/1.0.0/TooltipHelpText.vue';
import TooltipExternalLink from './components/ds/1.0/tooltips/1.0.0/TooltipExternalLink.vue';
import TooltipKpi from './components/ds/1.0/tooltips/1.0.0/TooltipKpi.vue';
import TooltipPeek from './components/ds/1.0/tooltips/1.0.0/TooltipPeek.vue';
import TooltipBase from './components/ds/1.0/tooltips/1.0.0/lib/TooltipBase.vue';
dsComponents.push(
    PrimitiveTooltip,
    TooltipIconLabel,
    TooltipHelpText,
    TooltipExternalLink,
    TooltipKpi,
    TooltipPeek,
    TooltipBase
);
import {
    getPointerFacing, 
    getPointerStyle,
} from './components/ds/1.0/tooltips/1.0.0/lib/TooltipBaseLib';

/* Typography */
import DisplayText from './components/ds/1.0/typography/1.0.0/DisplayText.vue';
import DisplaySubhead from './components/ds/1.0/typography/1.0.0/DisplaySubhead.vue';
import Heading from './components/ds/1.0/typography/1.0.0/Heading.vue';
import HeadingSupport from './components/ds/1.0/typography/1.0.0/HeadingSupport.vue';
import MarkedDown from './components/ds/1.0/typography/1.0.0/MarkedDown.vue';
import PreHead from './components/ds/1.0/typography/1.0.0/PreHead.vue';
import PreheadHeading from './components/ds/1.0/typography/1.0.0/PreheadHeading.vue';
import PreheadHeadingSupport from './components/ds/1.0/typography/1.0.0/PreheadHeadingSupport.vue';
import SupportingText from './components/ds/1.0/typography/1.0.0/SupportingText.vue';
import HeadingsBase from './components/ds/1.0/typography/1.0.0/HeadingsBase.vue';
import Paragraph from './components/ds/1.0/typography/1.0.0/Paragraph.vue';
dsComponents.push(
    DisplayText, 
    DisplaySubhead,
    Heading,
    HeadingSupport,
    MarkedDown,
    PreHead,
    PreheadHeading,
    PreheadHeadingSupport,
    SupportingText,
    HeadingsBase,
    Paragraph
);

export default {
  name: 'cabells-design-system',
  install(designsystem:any) {
    for (const compGroup in dsComponents) {
        const dsComponent = dsComponents[compGroup];
        if (typeof dsComponent == 'function') {
            continue;
        }
        if (!dsComponent.install || typeof dsComponent.install != 'function') {
            dsComponent.install = (app:any) => {
                app.component(dsComponent.name, dsComponent);
            };
        }
        designsystem.component(dsComponent.name, dsComponent);
    }
  }
};

export {
    formatTime, 
    format24HTime,
    formatAbsTime, 
    formatRelTime, 
    formatRelDate, 
    formatMonthDayYear, 
    formatAbbrevMonth,
    formatAbbrevDate,
    formatAbbrevDay,
    formatAbbrevDayTime,
    formatAbbrev24HDayTime,

    getIconClass,
    getIconCharFromCode,

    getBgColor,
    getBoxShadow,
    getMixBlendMode,
    getBgBlendMode,

    getBorderColor,
    getBorderOpacity,
    getBorderRadius,
    getBorderRingColor,
    getBorderRingWidth,
    getBorderStyle,
    getBorderWidth,

    getWidth,
    getMinWidth,
    getMaxWidth,
    getHeight,
    getMinHeight,
    getMaxHeight,
    getDisplay,
    getVisibility,
    getZIndex,
    getOverflow,
    getOverflowX,
    getOverflowY,
    getVerticalAlign,

    getItemAlign,
    getFlexDirection,
    getFlexWrap,

    getFontSize,
    getFontFamily,
    getFontWeight,
    getItalicStyle,
    getLeading,
    getTracking,
    getTextColor,
    getTextAlign,
    getTextDecoration,
    getTextTransform,
    getWhitespace,
    getCursor,

    getImgAlt,
    getImgDim,
    ImgAltDefault,

    getMargin, getMarginX, getMarginY,
    getMarginTop, getMarginBottom,
    getMarginRight, getMarginLeft,

    getOpacity,
    getBgOpacity,
    getTextOpacity,
    getDivideOpacity,
    getRingOpacity,

    getPadding,
    getPaddingTop,
    getPaddingBottom,
    getPaddingLeft,
    getPaddingRight,
    getPaddingX,
    getPaddingY, 

    getPosition,
    getTop,
    getBottom,
    getLeft,
    getRight,

    getGap,
    getGapX,
    getGapY,
    getSpaceX,
    getSpaceY,

    checkDefault,
    isValidStyle,
    parseState,
    getValidColor,
    getStyle,
    getAllowedStyle,
    getAllowedRootStyle,
    getOpenAccessColor,
    getOpenAccessOpacity,

    getValueByActive, 

    getPointerFacing, 
    getPointerStyle,

    getOrder, 
    getFlexAnimatable,
    getFlexWidth,
    getFlexStyle,
    getJustifyStyle,
    getAlignStyle,
    getAllowedList,
    getSmScreen,
    getDefScreen,

    /* Anchors */
    Anchor,

    /* Avatars */
    Avatar,
    TextAvatar,
    
    /* Buttons */
    ButtonBase,
    PrimitiveButton,
    PrimitiveCompactButton,
    PrimaryButton,
    PrimaryCompactButton,
    SecondaryButton,
    SecondaryCompactButton,
    TextButton,
    TextCompactButton,
    TextButtonSmall,
    HeroButton,
    HeroOutlineButton,
    ModalButton,
    ModalOutlineButton,
    ButtonGroup,
    ButtonGroupItem,
    ButtonTabGroup,
    ButtonTabItem,
    KabobGroup,
    SegmentedControl,
    ButtonSegment,
    NavsoButton,

    /* Cards */
    CardBase,
    JournalCardBase,
    JournalCardExpandBase,
    PrimitiveCard,
    JournalCard,
    JournalCardSmall,
    JournalCardNav,
	PredatoryJournalCardNav,
    JournalCardKabobMenu,
    PredatoryJournalCard,
    PredatoryJournalCardSmall,
    JournalCardExpand,
    PredatoryJournalCardExpand,
    PrimitiveJournalCardSection,
    CciSnapshot,
    KpiSnapshot,
    MetricSnapshot,
    PrimitiveSnapshot,
	PredatorySnapshotLarge,
	PredatoryOverviewStripe,
    PercentileBadge,
    DashboardSegment,
    JournalStatSegment,
    JournalStatGrouping,
    JournalStatRowGrouping,
    JournalStatAnchorGrouping,
    IconSegmentRight,


    /* Charts */
    DonutChart,

    /* Data */
    SimpleTime,
    Simple24HTime,
    MonthDayYear,
    AbsTime,
    RelTime,
    RelDate,
    DistantPast,
    Weekday,
    ShortMonth,
    ShortDate,
    ShortDay,
    ShortDayTime,
    Short24HDayTime,
    ShortText,
    HeatmapBase,
    Heatmap,
    HeatmapCell,
    HeatmapPreview,
    PrimitiveHeatmap,
    HeatmapGridType,
    HeatmapScoreType,
    HeatmapGridDisciplineType,
    CardSectionHeatmap,

    /* Dividers */
    SimpleDivider,
    VerticalDivider,
    TitleDivider,
    CaptionDivider,
    MenuItemDivider,

    /* Dropdowns */
    Dropdown,
    MenuItem,

    /* Flex */
    FlexCenter,
    FlexTopLeft,
    FlexCore,
    FlexStretch,
    Flexbox,
    FlexboxInline,
    FlexCell,
    FlexCellBase,
    FlexGrid,
    FlexGridLayout,
    FlexGridFull,
    FlexGridHalf,
    FlexGridThird,
    FlexGridTwoThirds,
    FlexGridFourth,
    FlexGridThreeFourths,
    FlexGridFifth,
    FlexGridTwoFifths,
    FlexGridThreeFifths,
    FlexGridFourFifths,
    FlexGridSixth,
    FlexGridFiveSixths,

    /* Grid */
    CdsGrid,
    GridCell,

    /* Icons */
    IconBase,
    PrimitiveIcon,
    Icon,
    IconXs,
    IconXl,
    Icon4,
    Icon6,
    Icon8,
    IconFontText,
    IconText,
    IconTooltip,

    /* Images */
    ImgBase,
    PrimitiveImage,
    ImageGradient,

    AltmetricsLogo,
    SciteLogo,
    CabellsLogo,

    GreenCheckCircle,
    BanCircle,
    HelpCircle,

    GreenOaBadge, 
    GreenOaBadgeSmall,

    /* Inputs */
    ToggleInputBase,
    PrimitiveToggleInput,
    PrimitiveTextInput,
    PrimitiveInputLabel,
    TextInput,
    TextInputBase,
    InputLabelBase,
    PrimitiveSelectInput,
    SelectInput,
    SelectTextInput,
    SelectInputBase,

    /* Lib */
    ComponentBase,
    BgBase,
    BorderBase,
    DimensionBase,
    FlexBase,
    FontBase,
    HoverBase,
    ToggleBase,
    ImageBase,
    MarginBase,
    PaddingBase,
    PositionBase,
    OpacityBase,
    SelectableBase,
    StyleBase,
    UtilityBase,
    PrimitiveDiv,
    ProgressIndicator,
    
    /* Pills */
    PillBase,
    PrimitivePill,
    FilterPill,

    /* Tags */
    SecondaryTagBase,
    TagBase,
    PrimitiveTag,
    PrimitiveCompactTag,
    PrimaryTag,
    PrimaryCompactTag,
    PredatoryTag,
    PredatoryCompactTag,
    PredatoryWarningTag,
    PredatoryWarningCompactTag,
    SecondaryTag,
    SecondaryCompactTag,
    IconTag,

    /* Tooltips */
    TooltipBase,
    PrimitiveTooltip,
    TooltipIconLabel,
    TooltipHelpText,
    TooltipExternalLink,
    TooltipKpi,
    TooltipPeek,

    /* Typography */
    DisplayText,
    DisplaySubhead,
    Heading,
    HeadingSupport,
    MarkedDown,
    PreHead,
    PreheadHeading,
    PreheadHeadingSupport,
    SupportingText,
    HeadingsBase,
    Paragraph,
}