
/**
 * Compares the current page pathname with the 
 * given list of page aliases to determine if 
 * they are the current page.
 * 
 * @param pages string[]
 * @returns boolean
 */
const isCurrentPage = (pages = ['']) : boolean => {
    if (pages.includes(document.location.pathname)) {
        return true;
    }
    return false;
};

const getCurrentPage = () : string => {
    return document.location.pathname;
};

export {
    isCurrentPage,
    getCurrentPage,
}