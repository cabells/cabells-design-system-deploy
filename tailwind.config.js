/*
 -- Tailwind will use the specified version of the design system
*/
const cabellstw = require('./design-system-config/1.0.0/tailwind.config');
cabellstw.prefix = 'cds-';
module.exports = cabellstw