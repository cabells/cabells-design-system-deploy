/**
 * This file serves the design system locally for testing
 */

import { createApp } from 'vue';
import App from './App.vue';
import axios from 'axios';
const marked = require('marked');
import { createWebHistory, createRouter  } from 'vue-router';
import WebAuth from "@auth0/auth0-spa-js";

function wait(duration: number) {
    return new Promise((resolve) => setTimeout(resolve, duration));
}

async function tryScrollToAnchor(hash: string, timeout = 600, delay = 150) {
    while (timeout > 0) {
      const el = document.querySelector(hash);
      if (el) {
        const headerOffset = 10;
        const elementPosition = el.getBoundingClientRect().top;
        const offsetPosition = elementPosition - headerOffset;
        window.scrollTo({
             top: offsetPosition,
             behavior: 'smooth',
        });
        break;
      }
      await wait(delay);
      timeout = timeout - delay;
    }
}

const routes = [
    {
        path: '/',
        name: '__home__',
        component: App,
    },
    {
        path: '/:pathMatch(.*)*', 
        name: 'NotFound', 
        component: App,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
    async scrollBehavior(to: any, from: any, savedPosition: any) {
        const selector = await to.hash;
        if (selector) {
            tryScrollToAnchor(to.hash, 1000, 100);
        } else if (savedPosition) {
            return savedPosition;
        } else {
            return { x: 0, y: 0 };
        }
    },
});

export default router;

const SITE = createApp(App);
SITE.config.globalProperties.window = window;
SITE.config.globalProperties.document = document;
SITE.config.globalProperties.console = console;
SITE.config.globalProperties.axios = axios;
SITE.config.globalProperties.marked = marked;
SITE.use(router);

if (process.env.VUE_APP_AUTH0_STATUS === 'enabled') {
    new (WebAuth as any)({
        domain: process.env.VUE_APP_AUTH_LOGIN_DOMAIN,
        audience: 'https://' + process.env.VUE_APP_AUTH_LOGIN_DOMAIN + '/api/v2/',
        client_id: process.env.VUE_APP_AUTH_LOGIN_CLIENT_ID,
        redirect_uri: process.env.VUE_APP_MEDICINE_URL,
        response_type: 'token id_token',
    }).then((auth0: any) => {
        SITE.config.globalProperties.auth0 = auth0;
        SITE.mount('#app');
    }).catch((e: any) => {
        if (process.env.NODE_ENV !== 'production') {
            console.error(e);
        }
    });
} else {
    SITE.mount('#app');
}