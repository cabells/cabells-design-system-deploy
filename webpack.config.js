const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const svgToMiniDataURI = require('mini-svg-data-uri');

const isProd = process.env.NODE_ENV === 'production';

module.exports = {
    plugins: [
        !isProd && new webpack.HotModuleReplacementPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [
                    'style-loader',
                    new MiniCssExtractPlugin(),
                    'css-loader',
                ],
            },
            {
                test: /\.(png|jpg|gif|ico)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            limit: 8192,
                        },
                    },
                ],
            },
            {
                test: /\.svg$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            generator: (content) => svgToMiniDataURI(content.toString()),
                        },
                    },
                ],
            },
            { 
                test: /\.tsx?$/, 
                loader: "ts-loader"
            },
            {
                test: /\.(eot|ttf|woff|woff2)(\?\S*)?$/,
                loader: "file-loader",
                options: {
                    name: "[name][contenthash:8].[ext]",
                },
            },
        ],
    },
    entry: {
        index: {
            import: path.resolve(__dirname, 'src/main.ts'),
            dependOn: 'all',
        },
        lib: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/lib/1.0.0/index.ts'),
            ],
        },
        anchors: {
            import: path.resolve(__dirname, 'src/components/ds/1.0/anchors/1.0.0/index.ts'),
            dependOn: 'lib',
        },
        avatars: {
            import: path.resolve(__dirname, 'src/components/ds/1.0/avatars/1.0.0/index.ts'),
            dependOn: 'lib',
        },
        buttons: {
            import: path.resolve(__dirname, 'src/components/ds/1.0/buttons/1.0.0/index.ts'),
            dependOn: ['lib', 'icons'],
        },
        cards: {
            import: path.resolve(__dirname, 'src/components/ds/1.0/cards/1.0.0/index.ts'),
            dependOn: 'lib',
        },
        data: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/index.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/heatmaps/index.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/heatmaps/lib/index.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/heatmaps/lib/HeatmapGridType.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/data/1.0.0/Format.ts'),
            ],
            dependOn: 'lib',
        },
        dividers: {
            import: path.resolve(__dirname, 'src/components/ds/1.0/dividers/1.0.0/index.ts'),
            dependOn: 'lib',
        },
        dropdown: {
            import: path.resolve(__dirname, 'src/components/ds/1.0/dropdown/1.0.0/index.ts'),
            dependOn: ['lib', 'buttons'],
        },
        flex: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/flex/1.0.0/index.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/flex/1.0.0/lib/FlexCellLib.ts'),
            ],
            dependOn: 'lib',
        },
        grid: {
            import: [
                path.resolve(__dirname, '/src/components/ds/1.0/grid/1.0.0/index.ts'),
                path.resolve(__dirname, '/src/components/ds/1.0/grid/1.0.0/lib/GridBaseLib.ts'),
                path.resolve(__dirname, '/src/components/ds/1.0/grid/1.0.0/lib/GridCellLib.ts'),
            ],
            dependOn: 'lib',
        },
        icons: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/icons/1.0.0/index.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/icons/1.0.0/lib/IconBaseLib.ts'),
            ],
            dependOn: 'lib',
        },
        images: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/images/1.0.0/index.ts'),
            ],
            dependOn: 'lib',
        },
        inputs: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/inputs/1.0.0/index.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/inputs/1.0.0/lib/InputBaseLib.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/inputs/1.0.0/lib/InputLabelBaseLib.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/inputs/1.0.0/lib/TextInputBaseLib.ts'),
            ],
            dependOn: ['lib', 'icons'],
        },
        tags: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/tags/1.0.0/index.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/tags/1.0.0/lib/SecondaryTagLib.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/tags/1.0.0/lib/TagBaseLib.ts'),
            ],
            dependOn: 'lib',
        },
        tooltips: {
            import: [
                path.resolve(__dirname, 'src/components/ds/1.0/tooltips/1.0.0/index.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/tooltips/1.0.0/lib/TooltipBaseLib.ts'),
                path.resolve(__dirname, 'src/components/ds/1.0/tooltips/1.0.0/lib/TooltipPeekLib.ts'),
            ],
            dependOn: ['lib', 'icons', 'tags'],
        },
        typography: {
            import: path.resolve(__dirname, 'src/components/ds/1.0/typography/1.0.0/index.ts'),
            // dependOn: 'lib',
        },
        twglobal: {
            import: path.resolve(__dirname, 'src/assets/css/ds/1.0/twglobal.css'),
            dependOn: [
                'styles',
                'keyframes',
            ],
        },
        configs: {
            import: [
                path.resolve(__dirname, 'design-system-config/1.0.0/animation.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/borderRadius.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/borderWidth.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/boxShadow.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/colors.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/flexGrow.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/flexShrink.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/fontFamily.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/fontSize.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/gridTemplateColumns.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/gridTemplateRows.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/letterSpacing.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/lineHeight.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/minmaxWidth.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/opacity.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/ringWidth.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/screens.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/spacing.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/zIndex.config.js'),
                path.resolve(__dirname, 'design-system-config/1.0.0/tailwind.config.js'),
            ],
        },
        styles: {
            import: [
                path.resolve(__dirname, 'src/assets/css/ds/1.0/journal-card.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/border.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/dropdown.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/flex.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/fonts.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/gap.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/gradient.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/grid.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/motion.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/shadow.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/snowbird-sharp-codes.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/tooltip.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/typography.css'),
            ],
        },
        keyframes: {
            import: [
                path.resolve(__dirname, 'src/assets/css/ds/1.0/keyframes/dropdown.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/keyframes/journal-card-preview.css'),
                path.resolve(__dirname, 'src/assets/css/ds/1.0/keyframes/spinners.css'),
            ],
        },
    },
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
        },
        mergeDuplicateChunks: true,
        maxInitialRequests: Infinity,
        minSize: 0,
        cacheGroups: {
            vendor: {
                test: /[\\/]node_modules[\\/]/,
                name(module) {
                    // get the name. E.g. node_modules/packageName/not/this/part.js
                    // or node_modules/packageName
                    const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                    // npm package names are URL-safe, but some servers don't like @ symbols
                    return `npm.${packageName.replace('@', '')}`;
                },
            },
        },
    },
    output: {
        filename: '[name].[contenthash].[ext]',
        path: path.resolve(__dirname, 'dist'),
        clean: 'true',
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: [".ts", ".tsx", ".js"]
    },
};