module.exports = {
    plugins: [
        require('postcss-import'),
        require('tailwindcss'),
        require('postcss-preset-env')(
            { 
                stage: 1, 
                features: {
                    // using postcss-preset-env can cause a problem:
                    // https://github.com/tailwindlabs/tailwindcss/issues/1190
                    // fix for `Error: Expected an opening square bracket.`
                    'focus-within-pseudo-class': false
                }
            }
        ),
        require('postcss-focus-visible'),
    ],
};